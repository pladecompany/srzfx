<div class="row">
	<div class="col-sm-12 col-md-8 offset-md-2">
		<div class="row">
			<div class="col-md-12 mb-2">
				<div class="title-box-d">
					<h3 class="title-d">Notificaciones</h3>
				</div>
			</div>
		</div>

		<div class="row mb-3">
			<div class="col s12">
				<a class="dropdown-item d-flex align-items-center p-0" href="#">
					<div class="mr-3">
						<div class="icon-circle bg-success">
							<i class="fas fa-donate text-white"></i>
						</div>
					</div>
					<div>
						<div class="small text-gray-500">Diciembre 7, 2019</div>
						$290.29 ha sido recibido en tu cuenta.
					</div>
				</a>
			</div>
		</div>
			
		<div class="row mb-3">
			<div class="col s12">
				<a class="dropdown-item d-flex align-items-center p-0" href="#">
					<div class="mr-3">
						<div class="icon-circle bg-success">
							<i class="fas fa-donate text-white"></i>
						</div>
					</div>
					<div>
						<div class="small text-gray-500">Diciembre 7, 2019</div>
						$290.29 ha sido recibido en tu cuenta.
					</div>
				</a>
			</div>
		</div>
			
		<div class="row mb-3">
			<div class="col s12">
				<a class="dropdown-item d-flex align-items-center p-0" href="#">
					<div class="mr-3">
						<div class="icon-circle bg-warning">
							<i class="fas fa-exclamation-triangle text-white"></i>
						</div>
					</div>
					<div>
						<div class="small text-gray-500">Diciembre 2, 2019</div>
						Esperando por el administrador
					</div>
				</a>
			</div>
		</div>

		<div class="row mb-3">
			<div class="col s12">
				<a class="dropdown-item d-flex align-items-center p-0" href="#">
					<div class="mr-3">
						<div class="icon-circle bg-success">
							<i class="fas fa-donate text-white"></i>
						</div>
					</div>
					<div>
						<div class="small text-gray-500">Diciembre 7, 2019</div>
						$290.29 ha sido recibido en tu cuenta.
					</div>
				</a>
			</div>
		</div>
			
		<div class="row mb-3">
			<div class="col s12">
				<a class="dropdown-item d-flex align-items-center p-0" href="#">
					<div class="mr-3">
						<div class="icon-circle bg-success">
							<i class="fas fa-donate text-white"></i>
						</div>
					</div>
					<div>
						<div class="small text-gray-500">Diciembre 7, 2019</div>
						$290.29 ha sido recibido en tu cuenta.
					</div>
				</a>
			</div>
		</div>
			
		<div class="row mb-3">
			<div class="col s12">
				<a class="dropdown-item d-flex align-items-center p-0" href="#">
					<div class="mr-3">
						<div class="icon-circle bg-warning">
							<i class="fas fa-exclamation-triangle text-white"></i>
						</div>
					</div>
					<div>
						<div class="small text-gray-500">Diciembre 2, 2019</div>
						Esperando por el administrador
					</div>
				</a>
			</div>
		</div>

		
	</div>
</div>