<?php //if(!isset($_GET['txt'])){ ?>
    <div class="row" style="margin-top:5em;">
      <div class="col-md-12">
        <h5 class="text-center">Busca lo que quieras en nuestra plataforma SRZ</h5>
      </div>
      <div class="col-md-4 offset-md-4" class="text-center">
        <form action="" method="GET">
          <input type="hidden" name="op" value="inicio">
          <input type="text" class="form-control" placeholder="Texto a buscar" name="txt" required min="3" style="height:60px;">
          <br>
          <center><input type="submit" name="btb" class="btn btn-success" value="Buscar"></center>
        </form>
      </div>
    </div>
    <?php }else if(isset($_GET['txt'])){?>
    <div class="row" style="margin-top:2em;">
      <div class="col-md-12">
        <h5 class="text-center">Resultados de la busqueda: "<?php echo $_GET['txt'];?>"</h5>
      </div>
    </div>
-->
    <?php
      include_once("modelo/Video.php");    
      $obj = new Video();
      $ro = $obj->fetchAllActivasByName($_GET['txt']);
    ?>
    <div class="row" style="margin-top:2em;">
      <h6 class="text-center">SRZ CLASES - RESULTADOS (<?php echo $ro->num_rows;?>)</h6>
      <div class="col-md-12">
        <div class="row">
          <?php
            while($fo = $ro->fetch_assoc()){
              echo "<div class='col-md-2'><u><a href='' style='font-size:14px;'>" . $fo['nom_vid']. "</a></u></div>";
            }
            if($ro->num_rows==0) echo 'No hay resultados.';
          ?>
        </div>
      </div>
    </div>
    <hr>
    <?php
      include_once("modelo/Proyeccion.php");    
      $obj = new Proyeccion();
      $ro = $obj->fetchAllActivasByName($_GET['txt']);
    ?>
    <div class="row" style="margin-top:2em;">
      <h6 class="text-center">PROYECCIONES - RESULTADOS (<?php echo $ro->num_rows;?>)</h6>
      <div class="col-md-12">
        <div class="row">
          <?php
