<?php 
    if(isset($_GET['msj'])){
      $msj = $_GET['msj'];
    if(isset($_GET['info']))  
    	$color='alert-info';
    else if(isset($_GET['err']))  
        $color='alert-warning';
?>

<div class="alert alert-secondary <?php echo $color;?>" role="alert" id="caja-msj" >
	<h5 class="text-center" id="" style=""><?php echo $msj;?></h5>
</div>

<script type="text/javascript">
    setTimeout(function(){
        $("#caja-msj").hide();
    }, 10000);
</script>
<?php } ?>
