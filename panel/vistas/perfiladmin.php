<?php
  include_once("controlador/perfil.php"); 
?>
<div class="card shadow mb-4 card-gen">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Perfil</h4>
	</div>

	<div class="card-body">
		<div class="row">
			<div class="col-sm-12 col-md-6 offset-md-3">
				<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_modelo">
					<div class="row">
						<div class="col s12 m2 offset-m5">
							<div class="text-center">
								<img class="img-perfil img-cover" src="../static/img/user.png" onerror="this.src='../static/img/user.png'" id="imagen">
							</div>
						</div>
					</div>


					<div class="row">
						<div class="col-md-12 mb-2">
							<div class="form-group">

								<label for="correo">Correo electrónico</label>
								<input type="text" name="cor_adm" class="form-control form-control-lg form-control-a" value="<?php echo $_SESSION['usuario'];?>" placeholder="Ingresa tu correo">
							</div>
							<?php 
								include_once("vistas/mensajes.php");                    
		                    ?>
						</div>
					</div>
					
					<div class="modal-footer">
						<button type="submit" id="bt_user" name="bt_user" class="btn btn-b">Guardar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>