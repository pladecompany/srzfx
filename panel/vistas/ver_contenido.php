<?php
  $tip = $_GET['tip']; 

  if($tip == 'bi'){
    include_once("modelo/Archivo.php");

    $arc = new Archivo();
    $farc = $arc->findById($_GET['id']);
    if($arc != false){

      if($farc['est_pdf']==1){
          $TIT = $farc['tit_pdf'];
          $DES = $farc['des_pdf'];
          $URL = $farc['url_pdf'];
      }else{
        echo "<script>window.location ='?op=inicio';</script>";
        exit(1);
      }
    }else{
        echo "<script>window.location ='?op=inicio';</script>";
        exit(1);
    }
  }else if($tip == 'pr'){
    include_once("modelo/Proyeccion.php");

    $arc = new Proyeccion();
    $farc = $arc->findById($_GET['id']);
    if($arc != false){

      if($farc['est_img']==1){
          $TIT = $farc['tit_img'];
          $DES = $farc['des_img'];
          $URL = $farc['url_img'];
          $FEC = $farc['fec_reg_img'];
      }else{
        echo "<script>window.location ='?op=inicio';</script>";
        exit(1);
      }
    }else{
        echo "<script>window.location ='?op=inicio';</script>";
        exit(1);
    }
  }else if($tip == 'cl'){
    include_once("modelo/Video.php");

    $arc = new Video();
    $farc = $arc->findById($_GET['id']);
    if($arc != false){

      if($farc['est_vid']==1){
          $TIT = $farc['nom_vid'];
          $DES = $farc['des_vid'];
          $URL = $farc['url_vid'];
          $TIV = $farc['tip_vid'];
          $FEC = $farc['fec_reg_vid'];
      }else{
        echo "<script>window.location ='?op=inicio';</script>";
        exit(1);
      }
    }else{
        echo "<script>window.location ='?op=inicio';</script>";
        exit(1);
    }
  }else{
    echo "<script>window.location ='?op=inicio';</script>";
    exit(1);
  }
?>

<?php if($tip == 'bi') { ?>
<div class="card shadow mb-4 pb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b"><?php echo $TIT;?></h4>
    </div>
    <div class="row">
      <div class="col-sm-12 col-md-12 text-justify mt-2">
        <p style="padding:2em;"><?php echo nl2br($DES);?></p>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 col-md-12 text-justify mt-2">
        <a href="<?php echo $URL;?>" target="__blank"><b style="padding:2em;">URL DE DESCARGA</b></a>
      </div>
    </div>
</div>

<?php }?>

<?php if($tip == 'cl') { ?>
<div class="card shadow mb-4 pb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b text-center"><?php echo $TIT;?></h4>
    </div>
    <div class="row">
      <div class="col-sm-12 col-md-12 mt-2 text-center">
        <?php if($TIV==1) {?>
          <video src="<?php echo $URL;?>" style="margin:auto;max-width:70%;width:100%;" controls>
        <?php }else if($TIV==0){?>
          <div><?php echo $URL;?></div>
        <?php }?>
      </div>
      <div class="col-sm-12 col-md-12 text-justify mt-2">
        <p style="padding:2em;"><?php echo nl2br($DES);?></p>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 col-md-12 text-justify mt-2">
        <b style="padding:2em;">PUBLICADO EL: <?php echo $FEC;?></b>
      </div>
    </div>
</div>

<?php }?>

<?php if($tip == 'pr') { ?>
<div class="card shadow mb-4 pb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b text-center"><?php echo $TIT;?></h4>
    </div>
    <div class="row">
      <div class="col-sm-12 col-md-12 mt-2 text-center">
          <img src="<?php echo $URL;?>" style="margin:auto;max-width:70%;width:100%;" controls>
      </div>
      <div class="col-sm-12 col-md-12 text-justify mt-2">
        <p style="padding:2em;"><?php echo nl2br($DES);?></p>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 col-md-12 text-justify mt-2">
        <b style="padding:2em;">PUBLICADO EL: <?php echo $FEC;?></b>
      </div>
    </div>
</div>

<?php }?>
