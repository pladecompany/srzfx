<?php
  include_once("controlador/usuario.php"); 
?>

<div class="card shadow mb-4 card-gen">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Perfil</h4>
	</div>

	<div class="card-body">
		<div class="row">
			<div class="col-sm-12 col-md-6 offset-md-3">
				<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_modelo">
					<div class="row">
						<div class="col s12 m2 offset-m5">
							<div class="text-center">
								<?php 
									//$imgper viene del archivo index
								?>
								<img class="img-perfil img-cover" src="../static/img/<?php echo $imgper;?>" onerror="this.src='../static/img/user.png'" id="imagen" style="max-width: 100%;">
							</div>
							
							<div class="text-center">
								<span>
									<label for="file" style="cursor: pointer;">
										<i class="fa fa-upload"> </i> Selecciona la imagen</label>
									<input type="file" id="file" onchange="nombrefile(this.value);" accept="image/*" name="img">
								</span>
								<br>
								<label class="name-img"></label>
								<br>
							</div>
						</div>
					</div>


					<div class="row">
						<div class="col-md-6 mb-2">
							<div class="form-group">
								<label for="Nombre">Nombre</label>
								<input type="text" name="nom_usu" required value="<?php echo $_SESSION['nom_usu'];?>" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu nombre">
							</div>
						</div>

						<div class="col-md-6 mb-2">
							<div class="form-group">
								<label for="Apellido">Apellido</label>
								<input type="text" name="ape_usu" required value="<?php echo $_SESSION['ape_usu'];?>" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu apellido">
							</div>
						</div>

						<div class="col-md-6 mb-2">
							<div class="form-group">
								<label for="nacimiento">Fecha de nacimiento</label>
								<?php 
									if($_SESSION['fec_nac_usu']==NULL || $_SESSION['fec_nac_usu']=='0000-00-00')
										$fecnac='';
									else
										$fecnac=$_SESSION['fec_nac_usu'];

									$fechanac = date('Y-m-d');
									$validfecha = strtotime ( '-10 year' , strtotime ( $fechanac ) ) ;
									$validfecha = date ( 'Y-m-d' , $validfecha );
								?>
								<input type="date" name="fec_usu" required max="<?php echo $validfecha;?>" value="<?php echo $fecnac;?>" class="form-control form-control-lg form-control-a">
							</div>
						</div>

						<div class="col-sm-12 col-md-6 mb-2">
							<div class="form-group">
								<label for="sexo">Sexo</label>
								<select name="sex_usu" class="form-control" required>
									<option value="">Seleccione</option>
									<option value="f" <?php if($_SESSION['sex_usu']=='f') echo "selected";?>>Femenino</option>
									<option value="m" <?php if($_SESSION['sex_usu']=='m') echo "selected";?>>Masculino</option>
								</select>
							</div>
						</div>

						<div class="col-sm-12 col-md-6 mb-2">
							<div class="form-group">
								<label for="Teléfono">Teléfono</label>
								<input type="number" name="tel_usu" value="<?php echo $_SESSION['tel_usu'];?>" max="99999999999999" required class="form-control form-control-lg form-control-a" placeholder="Ingresa tu teléfono">
							</div>
						</div>

						<div class="col-sm-12 col-md-6 mb-2">
							<div class="form-group">
								<label for="pais">Pais</label>
								<select name="pais" required class="form-control" required>
									<option value="">Seleccione</option>
									<?php 
										$ru=$orm->consultaPersonalizada("SELECT * FROM paises; ");
										while ($f=$ru->fetch_assoc()) {
											$selec='';
											if($_SESSION['id_pais']==$f['id'])
												$selec='selected';
									?>
										<option value="<?php echo $f['id']; ?>" <?php echo $selec; ?>><?php echo $f['pais']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="correo">Correo electrónico</label>
								<input type="email" required name="cor_usu" value="<?php echo $_SESSION['cor_usu'];?>" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu correo">
							</div>
							<?php 
								include_once("vistas/mensajes.php");                    
		                    ?>
						</div>
					</div>
					
					<div class="modal-footer">
						<div class="col-md-12 text-right">
							<button type="submit" id="bt_user" name="bt_user" class="btn btn-b">Guardar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	function nombrefile(fic) {
	  fic = fic.split('\\');
	  $('.name-img').html("Imagen seleccionada: "+fic[fic.length-1]);
	}
	$("#file").on('change', function() {

        if (this.files[0]) {
            var reader = new FileReader();
            reader.onloadend = function(evt) {
            	$('.img-perfil').attr('src',evt.target.result);
            };
            reader.readAsDataURL(this.files[0]);
        }

    });


</script>
