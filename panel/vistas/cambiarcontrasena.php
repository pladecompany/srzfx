<?php
  include_once("controlador/perfil.php"); 
?>
<div class="card shadow mb-4 card-gen">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Cambiar contraseña</h4>
	</div>

	<div class="card-body">
		<div class="row">
			<div class="col-sm-12 col-md-6 offset-md-3">
				<form class="form-a" method="POST" action="">
					<div class="row">
						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Contraseña">Ingresa tu contraseña actual</label>
								<input type="password" name="pass" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña">
							</div>
						</div>

						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Contraseña">Ingresa tu nueva contraseña</label>
								<input type="password" name="pass_new" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña">
							</div>
						</div>

						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Contraseña">Repite tu nueva contraseña</label>
								<input type="password" name="pass_rep" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña">
							</div>
							<?php 
								include_once("vistas/mensajes.php");                    
		                    ?>
						</div>

						<div class="col-md-12 text-right">
							<button type="submit" name="btncon" class="btn btn-b">Guardar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
