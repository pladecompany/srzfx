<?php
	if(isset($_POST['btncon'])){
		$archivo = "confi/costos.json";
		$data = file_get_contents($archivo); 
		$cos = json_decode($data, true);
		$cos["costos"]["valor"] = $_POST['pre_cos'];
        $cos["costos"]["descripcion"] = $_POST['des_cos'];
        if(file_put_contents($archivo, json_encode($cos, true))){
        	$err = "¡Información actualizada!";
      		echo "<script>window.location ='?op=costos&info&msj=$err';</script>";
        }
	}
	$archivo = "confi/costos.json";
	$data = file_get_contents($archivo); 
	$cos = json_decode($data, true);
	include_once("mensajes.php");
?>
<div class="card shadow mb-4 card-gen">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Costos</h4>
	</div>

	<div class="card-body">
		<div class="row">
			<div class="col-sm-12 col-md-6 offset-md-3">
				<form class="form-a" method="POST" action="">
					<div class="row">
						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="des_cos">Costo</label>
								<input value="<?php echo $cos['costos']['valor']; ?>" type="text" class="form-control form-control-lg form-control-a" name="pre_cos" id="pre_cos">
							</div>
						</div>
						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="des_cos" >Descripción</label>
								<textarea class="form-control form-control-lg form-control-a" name="des_cos" id="des_cos"><?php echo $cos['costos']['descripcion']; ?></textarea>
							</div>
						</div>
						<div class="col-md-12 text-right">
							<button type="submit" name="btncon" class="btn btn-b">Guardar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
