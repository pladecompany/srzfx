<?php
	include_once("modelo/Orm.php");
	include_once("vistas/mensajes.php");  
	if(isset($_GET['el'])){
	    $id = $_GET['el'];
        $r = $orm->eliminar("id", $id, "usuarios");
        if($r == true){
	      $err = "¡Eliminado correctamente!";
	      echo "<script>window.location ='?op=usuarios&info&msj=$err';</script>";
	    }else{
	      $err = "El registro no puede ser eliminado, tiene información en uso.";
	      echo "<script>window.location ='?op=usuarios&err&msj=$err';</script>";
	    }
	    exit(1);
	}

?>
<div class="card shadow mb-4 card-gen">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Usuarios</h4>
	</div>

	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Usuario</th>
						<th>Correo</th>
						<th>Estatus</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						$i=0;
						$ru=$orm->consultaPersonalizada("SELECT *, DATE_FORMAT(fec_nac_usu, '%d-%m-%Y') AS fecha_nac, (SELECT a.pais FROM paises a WHERE a.id=id_pais) as pais FROM usuarios; ");
						while ($f=$ru->fetch_assoc()) {
						$i++;	
							
					?>

					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $f['nom_usu'].' '.$f['ape_usu']; ?></td>
						<td><?php echo $f['cor_usu']; ?></td>
						<td>
							<div class="custom-control custom-switch">
								<?php 
									$che='';
									if($f['est_usu']==1)
										$che='checked';
								?>
								<input <?php echo $che; ?> type="checkbox" value="<?php echo $f['id']; ?>" class="custom-control-input sta_usu" id="sta_usu<?php echo $f['id']; ?>" >
								<label class="custom-control-label" for="sta_usu<?php echo $f['id']; ?>"></label>
							</div>
						</td>
						<td>
							<textarea style="display:none;" id='data_usu_<?php echo $i; ?>'><?php echo json_encode($f); ?></textarea>
							<a href="#" data-toggle="modal" data-target="#md-reestablecer" title="Restaurar contraseña" class="edit_cla" id="<?php echo $f['id']; ?>"><i class='mr-2 fa fa-lock'></i></a>
							<a href="#" data-toggle="modal" data-target="#md-ver" title="Ver usuario" class="ver_usu" id="<?php echo $i; ?>"><i class='mr-2 fa fa-eye'></i></a>
							<a href="#" class="eli_usu" id="<?php echo $f['id']; ?>"><i class='mr-2 fa fa-trash'></i></a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<div id="md-reestablecer" class="modal modalmedium fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="title-box-d">
					<h3 class="title-d" id="titulo_modulo">Reestablecer contraseña</h3>
				</div>

				<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_modelo">
					<input type="hidden" id="id_usu">
					<div class="row">
						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="con_usu">Escribe una nueva contraseña para éste usuario</label>
								<input type="password" class="form-control form-control-lg form-control-a" placeholder="Escribe la contraseña" name="con_usu" id="con_usu">
							</div>
						</div>

						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="con_usu">Repite la contraseña</label>
								<input type="password" class="form-control form-control-lg form-control-a" placeholder="Confirma la contraseña" name="con_usu_2" id="con_usu_2">
							</div>
						</div>
						<div class="col-md-12 mb-2">
							<h5 class="text-center" id="caja-msj-m"></h5>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="bt_can" name="bt_can" class="btn btn-b">Guardar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="md-ver" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="title-box-d">
					<h3 class="title-d" id="titulo_modulo">Ver usuario</h3>
				</div>
				<div id="info_usu"></div>
			</div>
			
			<div class="modal-footer">
				
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="static/js/funciones/usuario.js"></script>
