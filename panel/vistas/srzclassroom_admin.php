<?php
  include_once("controlador/videos.php");
  include_once("mensajes.php");

?>
<div class="card shadow mb-4 card-gen">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Clases</h4>
		<div class="text-right">
			<a href="#md-nuevaimagen" data-toggle="modal" class="color-b modal-trigger" id="bt_nueva_noticia"><b><i class="fa fa-plus-circle"></i> Registrar clase</b></a>
		</div>
	</div>

	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Título</th>
						<th>Categoría</th>
						<th>Estatus</th>
						<th>Fecha</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
                  <?php
                    $noti = new Video();
                    $r = $noti->fetchAll();
                    $i=0;
                    while($ff = $r->fetch_assoc()){
                      $i++;
                      echo "<tr>";
                      echo "  <td>" . $i . "</td>";
                      echo "  <td>" . $ff['nom_vid'] . "</td>";
                      echo "  <td>" . $ff['nom_cat'] . "</td>";
                      echo "  <td>" . (($ff['est_vid']==1)?'Visible':'No visible') . "</td>";
                      echo "  <td>" . $ff['fec_reg_vid'] . "</td>";
                      echo "<td><a href='?op=srzclassroom_admin&id=".$ff['idp']."'><i class='mr-2 fa fa-edit'></i></a>";
                      echo "<a href='?op=srzclassroom_admin&el=".$ff['idp']."' onclick='return confirm(\"¿ Esta seguro ?\")'><i class='mr-2 fa fa-trash'></i></a>";
                      echo "</td>";
                      echo "</tr>";
                    }
                  ?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<div id="md-nuevaimagen" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="title-box-d">
					<h3 class="title-d" id="titulo_modulo">Nueva clase</h3>
				</div>

				<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_modelo">
                    <?php if(isset($F)) echo "<input type='hidden' name='idn' value='".$F['id']."'>";?>
					<div class="row">
						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="tit_img">Título</label>
								<input type="text" class="form-control form-control-lg form-control-a" name="tit" id="tit" value="<?php if(isset($F)) echo $F['nom_vid'];?>">
							</div>
						</div>
						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="des_img">Descripción</label>
								<textarea class="form-control" id="des_img" name="des" rows="3"><?php if(isset($F)) echo nl2br($F['des_vid']);?></textarea>
							</div>
						</div>

						<div class="col-md-4 mb-2">
							<div class="form-group">
								<label for="fec_img">Categoría</label>
                                <select name="cat" class="form-control" required>
                                    <option value="" >Seleccione</option>
                                <?php
                                  $mod = new Categoria();
                                  $r = $mod->fetchAll();
                                  while($fm = $r->fetch_assoc()){
                                    if(isset($F) && $F['id_categoria'] == $fm['id']){
                                ?>
                                    <option value="<?php echo $fm['id'];?>" selected><?php echo strtoupper($fm['nom_cat']);?></option>
                                <?php }else{ ?>
                                    <option value="<?php echo $fm['id'];?>"><?php echo strtoupper($fm['nom_cat']);?></option>
                                <?php
                                      }
                                  }
                                ?>
                                </select>
							</div>
						</div>

						<div class="col-md-4 mb-2">
							<div class="form-group">
								<label for="fec_img">Estatus</label>
                                <select name="est" class="form-control" required>
                                    <option value="">Seleccione</option>
                                    <option value="1" <?php echo (isset($F) && $F['est_vid']==1)?'selected':'';?>>Publicación visible</option>
                                    <option value="0" <?php echo (isset($F) && $F['est_vid']==0)?'selected':'';?>>Publicación no visible</option>
                                </select>
							</div>
						</div>

						<div class="col-md-4 mb-2">
							<div class="form-group">
								<label for="fec_img">Tipo de video</label>
                                <select name="tip" class="form-control" required id="tipo_video">
                                    <option value="">Seleccione</option>
                                    <option value="1" <?php echo (isset($F) && $F['tip_vid']==1)?'selected':'';?>>ADJUNTAR</option>
                                    <option value="0" <?php echo (isset($F) && $F['tip_vid']==0)?'selected':'';?>>IFRAME/HTML</option>
                                </select>
							</div>
						</div>



						<div class="col-md-12 mb-2" id="conte_video_adjunto" style="<?php echo (isset($F) && $F['tip_vid']==1)?'':'display:none;';?>">
							<input type="file" class="custom-file-input" id="sub_img" name="img" accept="video/mp4,video/x-m4v,video/*">
							<label class="custom-file-label" for="sub_img"  style="width: 100%;">Subir video</label>

                            <?php 
                              if(isset($F) && $F["tip_vid"]==1) {
                             ?>

                              <video  src="<?php echo $F['url_vid'];?>" style="width:100%;" controls>

                            <?php 
                            
                              }
                            ?>
						</div>
						<div class="col-md-12 mb-2" id="conte_video_html" style="<?php echo (isset($F) && $F['tip_vid']==0)?'':'display:none;';?>">
							<div class="form-group">
								<label for="des_img">IFRAME/HTML</label>
								<textarea class="form-control" id="video_html" name="video_html" rows="3"><?php if(isset($F)) echo nl2br($F['url_vid']);?></textarea>
                                <br><br>
                                <?php 
                                
                                  if(isset($F) && $F["tip_vid"]==0) {
                                        echo "<div>" . $F["url_vid"] . "</div>";
                                  }
                                ?>
							</div>

						</div>
					</div>
				
					<div class="modal-footer">
				        <button type="submit" id="bt_modulo" name="<?php echo ((isset($F))?'btc':'btg')?>" class="btn btn-b"><?php echo ((isset($F))?'Guardar Cambios':'Guardar')?></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php
  if(isset($F)){
?>
  <script>
    $(document).ready(function(){
      $("#bt_nueva_noticia").trigger('click');
    });
  </script>

<?php
  } 
?>
<script>
    $(document).ready(function(){
      $("#bt_nueva_noticia").click(function(){
        $("#titulo_modulo").text("Nueva clase");
        $("#bt_modulo").attr('name', 'btg');
        $("#bt_modulo").text('Guardar');
        $("input[name='tit']").val('');
        $("textarea[name='des']").val('');
        $("select[name='est']").val('');
        $("select[name='cat']").val('');
        $("#img").attr("src", "../static/img/logo.png");
        $("#cont_img").remove();
      });

      $("#tipo_video").on('change', function(){
        var v = $("#tipo_video option:selected").val();
        if(v == 1){
          $("#conte_video_adjunto").show();
          $("#conte_video_html").hide();
        }else if(v==0){
          $("#conte_video_adjunto").hide();
          $("#conte_video_html").show();
        }else{
          $("#conte_video_adjunto").hide();
          $("#conte_video_html").hide();
        }
      });

    });

	function subirImg1(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#img')
				.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
</script>

