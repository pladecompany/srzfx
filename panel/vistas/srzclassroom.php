<?php
  include_once("modelo/Video.php");
  include_once("modelo/Categoria.php");

  if(isset($_GET['id'])){
    $id = $_GET['id'];

    $cat = new Categoria();
    $CAT = $cat->findById($id);
    if($CAT == false){
      echo "<script>window.location = '?op=srzclassroom';</script>";
      exit(1);
    }else{
      $tit = "CATEGORÍA: " . strtoupper($CAT['nom_cat']);
      $vid = new Video(); 
      $rvid = $vid->fetchAllActivasByCategory($id);
    }
    
  }else{
      $tit = "TODAS LAS CLASES";
      $vid = new Video(); 
      $rvid = $vid->fetchAllActivas();
  }
?>
<div class="card shadow mb-4 pb-4 card-gen">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b"><?php echo $tit;?></h4>

	<div class="row">
      <div class="col-sm-12 col-md-12 text-left mt-2">
          <label for="Título" style="">FILTRAR POR CATEGORÍA: </label>
          <a href="?op=srzclassroom" style="margin-right:1em;"><u>TODAS</u></a>
          <?php
            $mod = new Categoria();
            $r = $mod->fetchAll();
            while($fm = $r->fetch_assoc()){
           ?>
              <a href="?op=srzclassroom&id=<?php echo $fm['id'];?>" style="margin-right:1em;"><u><?php echo strtoupper($fm['nom_cat']);?></u></a>
          <?php
            }
          ?>
      </div>
    </div>
  </div>


	<div class="row">
        <?php

          while($fvid = $rvid->fetch_assoc()){
        ?>
          <div class="col-sm-3 mb-4 p-3">
              <a href="?op=ver_contenido&tip=cl&id=<?php echo $fvid['id'];?>">
              <div class="card" style="width: 100%;">
                  <div class="embed-responsive embed-responsive-16by9">
                      <?php
                        if($fvid['tip_vid'] == 1) { 
                          if($fvid['url_vid']==null||$fvid['url_vid'] == ''){
                            //echo "<img src='../static/img/logo.png' style='width:100%;'>";
                          }else{
                      ?>
                        <video src="<?php echo $fvid['url_vid'];?>" style="width:100%;" >
                      <?php 
                            }
                          }else if($fvid['tip_vid'] == 0){ 
                            if($fvid['url_vid']==null||$fvid['url_vid'] == ''){
                              //echo "<img src='../static/img/logo.png' style='width:100%;'>";
                            }else{
                              echo $fvid['url_vid'];
                            }

                          } ?>
                  </div>
                  <div class="card-body">
                      <h5 class="card-title text-center"><?php echo strtoupper($fvid['nom_vid']);?></h5>
                  </div>
              </div>
              </a>
          </div>
        <?php } ?>
        <?php
          if($rvid->num_rows==0){
            echo "<div class='col-md-12 text-bold text-center'>No hay resultados.</div>";
          }
        ?>


	</div>
</div>
