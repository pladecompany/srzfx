<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	
	$opcion = $_GET['op'];
	if($_SESSION['tipo']=='admin'){
		switch ($opcion) {
			case "inicio":
				$ruta="vistas/inicio.php";
			break;
			
			case "usuarios":
				$ruta="vistas/usuarios.php";
			break;
			
			case "categorias":
				$ruta="vistas/categorias.php";
			break;
			
			case "srzclassroom_admin":
				$ruta="vistas/srzclassroom_admin.php";
			break;
			
			case "biblioteca_admin":
				$ruta="vistas/biblioteca_admin.php";
			break;
			
			case "proyecciones_admin":
				$ruta="vistas/proyecciones_admin.php";
			break;
			case "perfil":
				$ruta="vistas/perfiladmin.php";
			break;
			case "costos":
				$ruta="vistas/costos.php";
			break;
			case "notificaciones":
				$ruta="vistas/notificaciones.php";
			break;
			case "cambiarcontrasena":
				$ruta="vistas/cambiarcontrasena.php";
			break;
			default:
				header('Location: ?op=inicio');
			break;
		}
	}else if($_SESSION['tipo']=='user'){
		switch ($opcion) {
			/*	usuario 	*/
			case "inicio":
				$ruta="vistas/inicio.php";
			break;	
            case "ver_contenido":
              $ruta = "vistas/ver_contenido.php";

            break;
			case "srzclassroom":
				$ruta="vistas/srzclassroom.php";
			break;

			case "biblioteca":
				$ruta="vistas/biblioteca.php";
			break;

			case "proyecciones":
				$ruta="vistas/proyecciones.php";
			break;

			case "perfil":
				$ruta="vistas/perfil.php";
			break;
			case "cambiarcontrasena":
				$ruta="vistas/cambiarcontrasena.php";
			break;
			default:
				header('Location: ?op=inicio');
			break;
		}
	}
?>
