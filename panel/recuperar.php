<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	include_once("modelo/Orm.php");
	include_once('ruta.php');
	if(isset($_POST['btc'])){
	  $dominio=$orm->obtenerDominio();
	  $email = $_POST['email'];
	  $pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ*#$@'-+"), 0, 10);
	  $sql = "UPDATE admins SET pas_adm=md5('".$pass."') WHERE cor_adm='$email';";
	  $r = $orm->editarPersonalizado($sql);
	  if($r == true){
		$mensaje='  
		<div style="background-color: #ca1724 !important;padding:.5em !important;color: #fff !important;font-weight: bold !important;margin-top: -1rem !important;box-shadow: 0px 5px 5px 0px #3d3c3c !important;">NUEVA CONTRASEÑA</div>

		<center>
		  <img src="'.$dominio.'/static/img/logo.png" style="width:30%;"><br>
		  <p style="color: #000 !important;font-size: 14px !important;"><b>CONTRASEÑA</b> '.$pass.'</p>
		</center>';
	  
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$orm->enviarCorreo($email, $mensaje, "NUEVA CLAVE DE ACCESO PARA SRZFX ADMIN");

		echo "<script>window.location = '?errr=0';</script>";
		exit(1);
	  }else{
		echo "<script>window.location = '?errr=1';</script>";
		exit(1);
	  }
	}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<title>SRZFX | Panel administrador</title>

	<link href="static/img/favicon.png" rel="icon">
	<link href="static/img/apple-touch-icon.png" rel="apple-touch-icon">

	<link href="static/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="static/css/panel.css" rel="stylesheet" type="text/css">
	<link href="static/css/style.css" rel="stylesheet" type="text/css">
</head>


<body class="bg-gradient-primary">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-lg-12 col-md-9">
				<div class="card o-hidden border-0 shadow-lg my-5">
					<div class="card-body p-0">
						<div class="row">
							<div class="col-lg-12">
								<div class="p-5">
									<form class="form-a" method="POST" action="">
										<div class="row">
											<div class="col-md-12 mb-2">
												<div class="title-box-d">
													<h3 class="title-d">Recuperar contraseña</h3>
												</div>
											</div>

											<div class="col-md-12 mb-2">
												<div class="form-group">
													<label for="Contraseña">Ingresa tu correo electrónico</label>
													<input type="email" name="email" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu correo electrónico">
												</div>
											</div>
											<?php 
												if(isset($_GET['errr'])){
												  $err = $_GET['errr'];
												  if($err == 1){
													$msj = "El correo no existe";
													$col = "color:red;";
												  }else{
													$msj = "Se ha cambiado tu contraseña, revisa tu correo electrónico";
													$col = "color:green;";
												  }
																					  
											?>
												<h5 style="<?php echo $col;?>" id="msj-error2"><?php echo $msj;?></h5>
												<?php } ?>
												<br><br>
											<div class="col-md-12">
												<a href="login.php" class="text-center clr_red">Iniciar sesión</a>
											</div>

											<div class="col-md-12 text-right">
												<button type="submit" name="btc" class="btn btn-b">Enviar</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="static/js/jquery.min.js"></script>
	<script src="static/js/bootstrap.bundle.min.js"></script>
	<script src="static/js/jquery.easing.min.js"></script>
	<script src="static/js/sb-admin-2.min.js"></script>
	<?php 
			if(isset($_GET['errr'])){
		?>
			<script type="text/javascript">
				$("#olvid-cla").trigger("click");
				setTimeout(function(){
					$("#msj-error2").hide();
				}, 5000);
			</script>
		<?php 
			}
		?>
</body>
</html>
