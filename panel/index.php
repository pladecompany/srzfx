<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	session_start();
	if(!isset($_SESSION['login']))
		echo "<script>window.location = '../';</script>";
	include_once('ruta.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="SRZ te impulsa al mercado de forex con asesorías y educación en línea.">
	<meta name="keywords" content="Forex, asesorías, educación financiera, negocios, trading, mercadeo">
	<meta name="author" content="SRZ">

	<title>SRZFX | Panel</title>
	<link rel="shortcut icon" href="../static/img/logo.ico" />
	<link href="../static/img/apple-touch-icon.png" rel="apple-touch-icon">

	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/css/all.min.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/css/panel.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/css/style.css">

	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/JTables/css/jquery.dataTables.min.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/JTables/css/dataTables.bootstrap.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/JTables/css/responsive.dataTables.min.css">

	<script src="static/js/jquery.min.js"></script>
</head>

<body id="page-top">
	<div id="wrapper">
		<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
			<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
				<div class="sidebar-brand-text mx-3">
					<img src="../static/img/logo.png" style="width: 100%">
				</div>
			</a>
			<li class="nav-item active">
				<a class="nav-link" href="?op=inicio">
				<i class="fas fa-fw fa-tachometer-alt"></i>
				<span>Inicio</span></a>
			</li>

			<?php if($_SESSION['tipo']=='admin'){ ?>

				<li class="nav-item">
					<a class="nav-link" href="?op=usuarios">
					<i class="fas fa-users"></i>
					<span>Usuarios</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="?op=categorias">
					<i class="fa fa-tag"></i>
					<span>Categorías</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="?op=srzclassroom_admin">
					<i class="fa fa-video"></i>
					<span>Srz classroom</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="?op=proyecciones_admin">
					<i class="fa fa-images"></i>
					<span>Proyecciones</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="?op=biblioteca_admin">
					<i class="fa fa-file-pdf"></i>
					<span>Biblioteca</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="?op=costos">
					<i class="fa fa-database"></i>
					<span>Costos</span></a>
				</li>

			<?php }else if($_SESSION['tipo']=='user'){ ?>


				<li class="nav-item">
					<a class="nav-link" href="?op=srzclassroom">
					<i class="fas fa-video"></i>
					<span>SRZ classroom</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="?op=proyecciones">
					<i class="fa fa-images"></i>
					<span>Proyecciones</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="?op=biblioteca">
					<i class="fa fa-file-pdf"></i>
					<span>Biblioteca</span></a>
				</li>
			<?php } ?>
			<div class="text-center d-none d-md-inline">
				<button class="rounded-circle border-0" id="sidebarToggle"></button>
			</div>

		</ul>

		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content">
				<nav class="navbar navbar-expand navbar-light bg-primary topbar mb-4 static-top shadow">
					<button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
						<i class="fa fa-bars"></i>
					</button>

					<ul class="navbar-nav ml-auto">
						<li class="nav-item dropdown no-arrow d-sm-none">
							<a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fas fa-search fa-fw"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
								<form class="form-inline mr-auto w-100 navbar-search">
									<div class="input-group">
										<input type="text" class="form-control border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
										<div class="input-group-append">
											<button class="btn btn-primary" type="button">
												<i class="fas fa-search fa-sm"></i>
											</button>
										</div>
									</div>
								</form>
							</div>
						</li>
						<?php if($_SESSION['tipo']=='admin'){ ?>
						<li class="nav-item dropdown no-arrow mx-1" style="display:none;">
							<a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fas fa-bell fa-fw"></i>
								<span class="badge badge-danger badge-counter">3+</span>
							</a>

							<div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
								<h6 class="dropdown-header">
									Notificaciones
								</h6>
								<a class="dropdown-item d-flex align-items-center" href="#">
									<div class="mr-3">
										<div class="icon-circle bg-primary">
											<i class="fas fa-file-alt text-white"></i>
										</div>
									</div>
									<div>
										<div class="small text-gray-500">Diciembre 12, 2019</div>
										<span class="font-weight-bold">Tienes una nueva notificación.</span>
									</div>
								</a>
								<a class="dropdown-item d-flex align-items-center" href="#">
									<div class="mr-3">
										<div class="icon-circle bg-success">
											<i class="fas fa-donate text-white"></i>
										</div>
									</div>
									<div>
										<div class="small text-gray-500">Diciembre 7, 2019</div>
										$290.29 ha sido recibido en tu cuenta.
									</div>
								</a>
								<a class="dropdown-item d-flex align-items-center" href="#">
									<div class="mr-3">
										<div class="icon-circle bg-warning">
											<i class="fas fa-exclamation-triangle text-white"></i>
										</div>
									</div>
									<div>
										<div class="small text-gray-500">Diciembre 2, 2019</div>
										Esperando por el administrador
									</div>
								</a>
								<a class="dropdown-item text-center small text-gray-500" href="?op=notificaciones">Leer más</a>
							</div>
						</li>
						<?php } ?>
						<li class="nav-item dropdown no-arrow mx-1">
							<a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="mr-2 d-none d-lg-inline text-white small"><?php echo $_SESSION['usuario'];?></span>
								<?php 
									if($_SESSION['img_usu']==NULL || $_SESSION['img_usu']=='')
										$imgper='user.png';
									else
										$imgper='user/'.$_SESSION['img_usu'];
								?>
								<img class="img-profile rounded-circle" src="../static/img/<?php echo $imgper;?>">
							</a>

							<div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
								<a class="dropdown-item d-flex align-items-center" href="?op=cambiarcontrasena">
									<div>
										<span class="font-weight-bold">Cambiar contraseña</span>
									</div>
								</a>
								<a class="dropdown-item d-flex align-items-center" href="?op=perfil">
									<div>
										<span class="font-weight-bold">Perfil</span>
									</div>
								</a>
								<a class="dropdown-item d-flex align-items-center" href="#" data-toggle="modal" data-target="#md-salir">
									<div>
										<span class="font-weight-bold">Salir</span>
									</div>
								</a>
							</div>
						</li>
					</ul>
				</nav>

				<div class="container-fluid">

					<?php include_once($ruta); ?>

				</div>

			</div>

			<footer class="sticky-footer bg-primary">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Copyright &copy; SRZFX <?php echo date('Y');?></span>
					</div>
				</div>
			</footer>
		</div>
	</div>

	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>

	<div id="md-salir" class="modal modalmedium fade" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="title-box-d">
						<h3 class="title-d" id="titulo_modulo">¿Desea cerrar sesión?</h3>
					</div>

					
					<div class="modal-footer">
						<a href="salir.php" name="" class="btn btn-b btn-red solid">Sí</a>
						<a href="" name="" class="btn btn-b btn-red solid">No</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="static/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="static/js/jquery.easing.min.js"></script>
	<script type="text/javascript" src="static/js/sb-admin-2.min.js"></script>
	<script type="text/javascript" src="static/lib/JTables/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="static/lib/JTables/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="static/lib/JTables/js/dataTables.responsive.min.js"></script>

	<script>
		$(document).ready(function(){
			$('.table').DataTable( {
				"language": {
					"url": "static/lib/JTables/Spanish.json"
				}
			} );
			$('.table').DataTable( {
				responsive: true
			} );
		} );
	</script>
</body>
</html>
