$(document).ready(function(){
	var controlmsj = null;
	$(document).on('change', ".sta_usu", function(){
		valor=0;
		if( $(this).is(':checked') ){
	        valor=1;
	    } 
	    var data = {
        	editsts: 1,
        	idu: $(this).val(),
	        valor: valor
	    }

	    $.ajax({
	        url: "modelo/modulousuario.php",
	       	type: "POST",
	        data: data,
	        dataType: "json",

	          success: function(data){
	          	//console.log(data);
	          	if(data==0)
	            	alert("Error al editar estatus, intenta mas tarde");
	          },
	          error: function(xhr, status, errorThrown){
	            console.log(xhr);
	          }
	      });

	});
	$(document).on('click', ".ver_usu", function(){
		valor=$(this).attr("id");
		data=jQuery.parseJSON($("#data_usu_"+valor).val());
		//console.log(data);
		if(data.sex_usu=='f')
			sexo='Femenino';
		else
			sexo="Masculino";
		if(data.pais!=null)
			pais=data.pais;
		else
			pais='';
		if(data.img_usu==null || data.img_usu=='')
			img='user.png';
		else
			img='user/'+data.img_usu;
		var onerror="this.src='../static/img/user.png'";
		html="<ul class='list-group'>"+
                '<li class="list-group-item text-center"><img src="../static/img/'+img+'" onerror="'+onerror+'" class="img-perfil img-cover"/></li>'+
                "<li class='list-group-item'><b>Nombre:</b> "+data.nom_usu+" "+data.ape_usu+"</li>"+
                "<li class='list-group-item'><b>Correo:</b> "+data.cor_usu+"</li>"+
                "<li class='list-group-item'><b>Teléfono:</b> "+data.tel_usu+"</li>"+
                "<li class='list-group-item'><b>Fecha de Nacimiento:</b> "+data.fecha_nac+"</li>"+
                "<li class='list-group-item'><b>País:</b> "+pais+"</li>"+
                "<li class='list-group-item'><b>Sexo:</b> "+sexo+"</li>"+
            "</ul>";
		$("#info_usu").html(html);
	});

	$(document).on('click', ".eli_usu", function(){
		valor=$(this).attr("id");
		if (confirm("Desea eliminar este usuario?")) {
		 	window.location = '?op=usuarios&el='+valor;
		} 
	});
	$(document).on('click', ".edit_cla", function(){
		valor=$(this).attr("id");
		$("#id_usu").val(valor);
	});
	$(document).on('keyup', "#con_usu , #con_usu_2", function(){
		$("#caja-msj-m").html('');
		clearTimeout(controlmsj);
	});
	$(document).on('click', "#bt_can", function(){
		if ($("#con_usu").val().trim().length<5) {
			$("#caja-msj-m").html("La contraseña debe tener mínimo 5 carácteres");
			$("#caja-msj-m").css('color','red');
			controlmsj = setTimeout(function(){$("#caja-msj-m").html('');}, 5000);
		}else if ($("#con_usu_2").val().trim().length<5) {
			$("#caja-msj-m").html("La contraseña a repetir debe tener mínimo 5 carácteres");
			$("#caja-msj-m").css('color','red');
			controlmsj = setTimeout(function(){$("#caja-msj-m").html('');}, 5000);
		}else if ($("#con_usu").val()!=$("#con_usu_2").val()) {
			$("#caja-msj-m").html("Las contraseñas no coinciden");
			$("#caja-msj-m").css('color','red');
			controlmsj = setTimeout(function(){$("#caja-msj-m").html('');}, 5000);
		}else{
			var data = {
	        	editcon: 1,
	        	idu: $("#id_usu").val(),
		        valor: $("#con_usu").val()
		    }

		    $.ajax({
		        url: "modelo/modulousuario.php",
		       	type: "POST",
		        data: data,
		        dataType: "json",

		          success: function(data){
		          	//console.log(data);
		          	if(data==1){
		          		$("#con_usu").val('');
		          		$("#con_usu_2").val('');
		          		$("#caja-msj-m").html("La contraseña se restauro correctamente");
						$("#caja-msj-m").css('color','green');
						controlmsj = setTimeout(function(){$("#caja-msj-m").html('');}, 5000);
		          	}else{
		          		$("#caja-msj-m").html("Error al restaurar contraseña/Contraseña actual");
						$("#caja-msj-m").css('color','red');
						controlmsj = setTimeout(function(){$("#caja-msj-m").html('');}, 5000);
		          	}
		            
		          },
		          error: function(xhr, status, errorThrown){
		            console.log(xhr);
		          }
		      });
			
		}
	});
});