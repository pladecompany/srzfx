<?php
  include_once("modelo/Usuario.php"); 

  if(isset($_POST) && isset($_POST['bt_user'])){

    $nom = $_POST['nom_usu'];
    $ape = $_POST['ape_usu'];
    $tel = $_POST['tel_usu'];
    $cor = $_POST['cor_usu'];
    $sex = $_POST['sex_usu'];
    $fec = $_POST['fec_usu'];
    $pais = $_POST['pais'];
    $fechanac = date('Y-m-d');
    $validfecha = strtotime ( '-10 year' , strtotime ( $fechanac ) ) ;
    $validfecha = date ( 'Y-m-d' , $validfecha );
      
    
    if(strlen($nom) < 2){
      $err = "El campo nombre debe tener al menos 2 carácteres";
    }else if(strlen($ape) < 2){
      $err = "El campo apellido debe tener al menos 2 carácteres";
    }else if($validfecha < $fec){
      $err = "Debes tener mas de 10 años para registrarte";
    }else if(strlen($tel) < 10 || strlen($tel)>14){
      $err = "El campo teléfono debe tener un formato valido";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=perfil&err&msj=$err';</script>";
      exit(1);
    }

    $usuario = new Usuario();
    $usuario->data["nom_usu"] = $nom;
    $usuario->data["ape_usu"] = $ape;
    $usuario->data["cor_usu"] = $cor;
    $usuario->data["tel_usu"] = $tel;
    $usuario->data["sex_usu"] = $sex;
    $usuario->data["id_pais"] = $pais;
    $usuario->data["fec_nac_usu"] = $fec;

    $ruta = "../static/img/user";
    $img1 = $_FILES['img']['tmp_name'];
    $nom1 = $_FILES['img']['name'];
    $rim = "";
    if(!empty($_FILES['img']['name'])){
      $rim = $orm->subirArchivo($img1, $nom1, $ruta);
      $rim = str_replace("../static/img/user/", "", $rim);
      $usuario->data["img_usu"] = $rim;
    }

    
    $id = $_SESSION['idusu'];
    $r = $usuario->edit($id);
    if($r==true){
      $err = "¡Información actualizada!";
      $_SESSION['usuario'] = $nom.' '.$ape;
      $_SESSION['nom_usu'] = $nom;
      $_SESSION['ape_usu'] = $ape;
      $_SESSION['fec_nac_usu'] = $fec;
      $_SESSION['cor_usu'] = $cor;
      $_SESSION['sex_usu'] = $sex;
      $_SESSION['tel_usu'] = $tel;
      $_SESSION['id_pais'] = $pais;
      if($rim!='')
        $_SESSION['img_usu'] = $rim;
      echo "<script>window.location ='?op=perfil&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=perfil&err&msj=$err';</script>";
    }
    exit(1);


  }

?>
