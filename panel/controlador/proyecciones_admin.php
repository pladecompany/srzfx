<?php
  include_once("modelo/Proyeccion.php"); 
  include_once("modelo/Categoria.php"); 
  include_once("modelo/Conexion.php"); 

  if(isset($_POST) && isset($_POST['btg'])){

    $tit = $_POST['tit'];
    $idc = $_POST['cat'];
    $est = $_POST['est'];
    $des = $_POST['des'];

    $cliente = new Proyeccion();
    $cliente->data["id"] = "";
    $cliente->data["id_categoria"] = $idc;
    $cliente->data["tit_img"] = $tit;
    $cliente->data["des_img"] = $des;
    $cliente->data["url_img"] = null;
    $cliente->data["fec_reg_img"] = date('Y-m-d H:i:s');
    $cliente->data["est_img"] = $est;


    $r = $cliente->save();
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      
      if(!empty($_FILES['img'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/files/".$nom1;
        if(move_uploaded_file($_FILES['img']['tmp_name'], $nombre)) {
          $producto = new Proyeccion();
          $producto->data['url_img'] = $nf;
          $producto->edit($id);
        }
      }

      $err = "¡Registró correctamente!";
      echo "<script>window.location ='?op=proyecciones_admin&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "¡Código/Correo ya existe!";
      echo "<script>window.location ='?op=proyecciones_admin&err&msj=$err';</script>";
      exit(1);
    }

  }else if(isset($_POST) && isset($_POST['btc'])){

    $tit = $_POST['tit'];
    $idc = $_POST['cat'];
    $est = $_POST['est'];
    $des = $_POST['des'];

    $cliente = new Proyeccion();

    $id = $_POST['idn'];
    $cliente->data["id"] = $id;
    $cliente->data["id_categoria"] = $idc;
    $cliente->data["tit_img"] = $tit;
    $cliente->data["des_img"] = $des;
    $cliente->data["est_img"] = $est;


    if(!empty($_FILES['img'])){
      $orm = new Orm(new Conexion());
      $ruta = getcwd() . "/../static/files/";
      $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img']['name']); 
      $nombre = $ruta . $nom1;
      $nf = $orm->obtenerDominio()."/static/files/".$nom1;
      if(move_uploaded_file($_FILES['img']['tmp_name'], $nombre)) {
        $producto = new Proyeccion();
        $producto->data['url_img'] = $nf;
        $producto->edit($id);
      }
    }

    $r = $cliente->edit($id);
    if($r==true){
      $err = "¡Información actualizada!";
      echo "<script>window.location ='?op=proyecciones_admin&id=$id&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=proyecciones_admin&id=$id&info&msj=$err';</script>";
    }
    exit(1);


  }else if(isset($_GET['id'])){
    $id = $_GET['id'];
    $cliente = new Proyeccion();
    $r = $cliente->findById($id);
    if($r==false){
      $err = "No existe ningún registro con el ID ($id).";
      echo "<script>window.location ='?op=proyecciones_admin&err&msj=$err';</script>";
      exit(1);
    }else{
      $F = $r;
    }
  }else if(isset($_GET['el'])){
    $id = $_GET['el'];
    $cliente = new Proyeccion();
    if($cliente->removeById($id)){
      $err = "¡Eliminado correctamente!";
      echo "<script>window.location ='?op=proyecciones_admin&info&msj=$err';</script>";
    }else{
      $err = "El registro no puede ser eliminado, tiene información en uso.";
      echo "<script>window.location ='?op=proyecciones_admin&err&msj=$err';</script>";
    }
    exit(1);
  }

?>
