<?php
  include_once("modelo/Vehiculo.php"); 
  include_once("modelo/Modelo.php"); 
  include_once("modelo/Conexion.php"); 

  if(isset($_POST) && isset($_POST['btg'])){

    $nom = $_POST['nom'];
    $idm = $_POST['mod'];
    $ano = $_POST['ano'];
    $pre = $_POST['pre'];
    $tra = $_POST['tra'];
    $est = $_POST['est'];
    $imp = $_POST['imp'];
    $sto = $_POST['sto'];
    $des = $_POST['des'];
    $fle = $_POST['fle'];
    $dis = $_POST['dis'];
    $seg = $_POST['seg'];

    /*
    if(strlen($tit) == 0){
      $err = "Debe llenar el campo titulo.";
    }else if(strlen($des) < 10){
      $err = "El campo descripción debe tener al menos 10 carácteres";
    }else if(strlen($est) == 0){
      $err = "Debe seleccionar el estatu";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=vehiculos&err&msj=$err';</script>";
      exit(1);
    }
    */

    $cliente = new Vehiculo();
    $cliente->data["id"] = "";
    $cliente->data["id_modelo"] = $idm;
    $cliente->data["nom_veh"] = $nom;
    $cliente->data["ano_veh"] = $ano;
    $cliente->data["pre_veh"] = $pre;
    $cliente->data["tra_veh"] = $tra;
    $cliente->data["est_veh"] = $est;
    $cliente->data["imp_veh"] = $imp;
    $cliente->data["sto_veh"] = $sto;
    $cliente->data["des_veh"] = $des;
    $cliente->data["fle_veh"] = $fle;
    $cliente->data["dis_veh"] = $dis;
    $cliente->data["seg_veh"] = $seg;
    $cliente->data["img1"] = null;
    $cliente->data["img2"] = null;
    $cliente->data["img3"] = null;
    $cliente->data["img4"] = null;
    $cliente->data["img5"] = null;

    $r = $cliente->save();
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      
      if(!empty($_FILES['img1'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img1']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img1']['tmp_name'], $nombre)) {
          $producto = new Vehiculo();
          $producto->data['img1'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img2'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img2']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img2']['tmp_name'], $nombre)) {
          $producto = new Vehiculo();
          $producto->data['img2'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img3'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img3']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img3']['tmp_name'], $nombre)) {
          $producto = new Vehiculo();
          $producto->data['img3'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img4'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img4']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img4']['tmp_name'], $nombre)) {
          $producto = new Vehiculo();
          $producto->data['img4'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img5'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img5']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img5']['tmp_name'], $nombre)) {
          $producto = new Vehiculo();
          $producto->data['img5'] = $nf;
          $producto->edit($id);
        }
      }

      $err = "¡Registró correctamente!";
      echo "<script>window.location ='?op=vehiculos&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "¡Código/Correo ya existe!";
      echo "<script>window.location ='?op=vehiculos&err&msj=$err';</script>";
      exit(1);
    }

  }else if(isset($_POST) && isset($_POST['btc'])){

    $nom = $_POST['nom'];
    $idm = $_POST['mod'];
    $ano = $_POST['ano'];
    $pre = $_POST['pre'];
    $tra = $_POST['tra'];
    $est = $_POST['est'];
    $imp = $_POST['imp'];
    $sto = $_POST['sto'];
    $des = $_POST['des'];
    $fle = $_POST['fle'];
    $dis = $_POST['dis'];
    $seg = $_POST['seg'];

    $cliente = new Vehiculo();
    $id = $_POST['idn'];
    $cliente->data["id"] = $id;
    $cliente->data["id_modelo"] = $idm;
    $cliente->data["nom_veh"] = $nom;
    $cliente->data["ano_veh"] = $ano;
    $cliente->data["pre_veh"] = $pre;
    $cliente->data["tra_veh"] = $tra;
    $cliente->data["est_veh"] = $est;
    $cliente->data["imp_veh"] = $imp;
    $cliente->data["sto_veh"] = $sto;
    $cliente->data["des_veh"] = $des;
    $cliente->data["fle_veh"] = $fle;
    $cliente->data["dis_veh"] = $dis;
    $cliente->data["seg_veh"] = $seg;

      if(!empty($_FILES['img1'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img1']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img1']['tmp_name'], $nombre)) {
          $producto = new Vehiculo();
          $producto->data['img1'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img2'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img2']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img2']['tmp_name'], $nombre)) {
          $producto = new Vehiculo();
          $producto->data['img2'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img3'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img3']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img3']['tmp_name'], $nombre)) {
          $producto = new Vehiculo();
          $producto->data['img3'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img4'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img4']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img4']['tmp_name'], $nombre)) {
          $producto = new Vehiculo();
          $producto->data['img4'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img5'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img5']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img5']['tmp_name'], $nombre)) {
          $producto = new Vehiculo();
          $producto->data['img5'] = $nf;
          $producto->edit($id);
        }
      }

    $r = $cliente->edit($id);
    if($r==true){
      $err = "¡Información actualizada!";
      echo "<script>window.location ='?op=vehiculo&id=$id&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=vehiculo&id=$id&info&msj=$err';</script>";
    }
    exit(1);


  }else if(isset($_GET['id'])){
    $id = $_GET['id'];
    $cliente = new Vehiculo();
    $r = $cliente->findById($id);
    if($r==false){
      $err = "No existe ningún registro con el ID ($id).";
      echo "<script>window.location ='?op=vehiculos&err&msj=$err';</script>";
      exit(1);
    }else{
      $F = $r;
    }
  }else if(isset($_GET['el'])){
    $id = $_GET['el'];
    $cliente = new Vehiculo();
    if($cliente->removeById($id)){
      $err = "¡Eliminado correctamente!";
      echo "<script>window.location ='?op=vehiculos&info&msj=$err';</script>";
    }else{
      $err = "El registro no puede ser eliminado, tiene información en uso.";
      echo "<script>window.location ='?op=vehiculos&err&msj=$err';</script>";
    }
    exit(1);
  }

?>
