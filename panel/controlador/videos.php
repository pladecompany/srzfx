<?php
  include_once("modelo/Video.php"); 
  include_once("modelo/Categoria.php"); 
  include_once("modelo/Conexion.php"); 

  if(isset($_POST) && isset($_POST['btg'])){

    $tit = $_POST['tit'];
    $idc = $_POST['cat'];
    $est = $_POST['est'];
    $des = $_POST['des'];
    $tip = $_POST['tip'];

    $cliente = new Video();
    $cliente->data["id"] = "";
    $cliente->data["id_categoria"] = $idc;
    $cliente->data["nom_vid"] = $tit;
    $cliente->data["des_vid"] = $des;
    $cliente->data["url_vid"] = null;
    $cliente->data["est_vid"] = $est;
    $cliente->data["tip_vid"] = $tip;
    $cliente->data["fec_reg_vid"] = date('Y-m-d H:i:s');



    if($tip == 0)
      $cliente->data["url_vid"] = $_POST['video_html'];


    $r = $cliente->save();
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      
      if(!empty($_FILES['img']) && $tip == 1){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/files/".$nom1;
        if(move_uploaded_file($_FILES['img']['tmp_name'], $nombre)) {
          $producto = new Video();
          $producto->data['url_vid'] = $nf;
          $producto->edit($id);
        }
      }

      $err = "¡Registró correctamente!";
      echo "<script>window.location ='?op=srzclassroom_admin&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "¡Código/Correo ya existe!";
      echo "<script>window.location ='?op=srzclassroom_admin&err&msj=$err';</script>";
      exit(1);
    }

  }else if(isset($_POST) && isset($_POST['btc'])){

    $tit = $_POST['tit'];
    $idc = $_POST['cat'];
    $est = $_POST['est'];
    $des = $_POST['des'];
    $tip = $_POST['tip'];

    $cliente = new Video();

    $id = $_POST['idn'];
    $cliente->data["id"] = $id;
    $cliente->data["id_categoria"] = $idc;
    $cliente->data["nom_vid"] = $tit;
    $cliente->data["des_vid"] = $des;
    $cliente->data["est_vid"] = $est;
    $cliente->data["tip_vid"] = $tip;

    if($tip == 0)
      $cliente->data["url_vid"] = $_POST['video_html'];


    if(!empty($_FILES['img']) && $tip == 1){
      $orm = new Orm(new Conexion());
      $ruta = getcwd() . "/../static/files/";
      $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img']['name']); 
      $nombre = $ruta . $nom1;
      $nf = $orm->obtenerDominio()."/static/files/".$nom1;
      if(move_uploaded_file($_FILES['img']['tmp_name'], $nombre)) {
        $producto = new Video();
        $producto->data['url_vid'] = $nf;
        $producto->edit($id);
      }
    }

    $r = $cliente->edit($id);
    if($r==true){
      $err = "¡Información actualizada!";
      echo "<script>window.location ='?op=srzclassroom_admin&id=$id&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=srzclassroom_admin&id=$id&info&msj=$err';</script>";
    }
    exit(1);


  }else if(isset($_GET['id'])){
    $id = $_GET['id'];
    $cliente = new Video();
    $r = $cliente->findById($id);
    if($r==false){
      $err = "No existe ningún registro con el ID ($id).";
      echo "<script>window.location ='?op=srzclassroom_admin&err&msj=$err';</script>";
      exit(1);
    }else{
      $F = $r;
    }
  }else if(isset($_GET['el'])){
    $id = $_GET['el'];
    $cliente = new Video();
    if($cliente->removeById($id)){
      $err = "¡Eliminado correctamente!";
      echo "<script>window.location ='?op=srzclassroom_admin&info&msj=$err';</script>";
    }else{
      $err = "El registro no puede ser eliminado, tiene información en uso.";
      echo "<script>window.location ='?op=srzclassroom_admin&err&msj=$err';</script>";
    }
    exit(1);
  }

?>
