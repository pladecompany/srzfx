<?php
  include_once("modelo/Pedido.php"); 
  include_once("modelo/Orm.php"); 
  include_once("modelo/Conexion.php"); 

  if(!isset($_GET['id'])){
    $fac = new Pedido(); 
    $all = $fac->fetchAll();
  }

  if(isset($_GET['id'])){
    $fac = new Pedido();
    $FF = $fac->findById($_GET['id']);
    if($FF == false){
      echo "<script>window.location ='?op=pedidos&err&msj=Ésta factura no existe.';</script>";
      exit(1);
    }
    $all = $fac->fetchDetalles($_GET['id']);
  }else if(isset($_GET['check'])){
    include_once("modelo/Inventario.php"); 
    $id = $_GET['check'];
    $ped = new Pedido();
    $ped->data["est_ped"] = 1;
    $r = $ped->edit($id);
    if($r){
      $ped = new Pedido();
      $dts = $ped->fetchDetalles2($_GET['check']);
      while($f = $dts->fetch_assoc()){
        $inv = new Inventario();
        $pro = $inv->findById($f['id_producto']);
        if($pro != false){
          $inv->data["can_pro"] = $pro['can_pro'] - $f['can_pro'];
          $inv->edit($f['id_producto']);
        }
      }
      echo "<script>window.location ='?op=pedidos&info&msj=Pedido procesado correctamente.';</script>";
    }else{
      echo "<script>window.location ='?op=pedidos&err&msj=No se pudo procesar el pedido.';</script>";
    }
    exit(1);
  }else if(isset($_GET['trash'])){
    $ped = new Pedido();
    $r = $ped->removeById($_GET['trash']);
    if($r){
      echo "<script>window.location ='?op=pedidos&info&msj=Pedido eliminado correctamente.';</script>";
    }else{
      echo "<script>window.location ='?op=pedidos&info&msj=El registro no puede ser eliminado, tiene información en uso.';</script>";
    }
    exit(1);
  }
  if(isset($_GET['noti'])){
    $id = $_GET['id'];
    $ped = new Pedido();
    $ped->data["visto"]=1;
    $ped->edit($id);
  }


?>
