<?php
  include_once("modelo/Admin.php"); 

  if(isset($_POST) && isset($_POST['bt_user'])){

    $cor = $_POST['cor_adm'];


    $admin = new Admin();
    $admin->data["cor_adm"] = $cor;
    $id = $_SESSION['idusu'];
    $r = $admin->edit($id);
    if($r==true){
      $err = "¡Información actualizada!";
      $_SESSION['usuario'] = $cor;
      echo "<script>window.location ='?op=perfil&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=perfil&err&msj=$err';</script>";
    }
    exit(1);


  }
  if(isset($_POST) && isset($_POST['btncon'])){

    $pa = $_POST['pass'];
    $pn = $_POST['pass_new'];
    $rp = $_POST['pass_rep'];

    if(strlen($pa) < 5){
      $err = "La contraseña actual debe tener mínimo 5 carácteres";
    }else if(strlen($pn) < 5){
      $err = "La contraseña nueva debe tener mínimo 5 carácteres";
    }else if(strlen($rp) < 5){
      $err = "La contraseña a repetir debe tener mínimo 5 carácteres";
    }else if($rp != $pn){
      $err = "Las contraseñas no coinciden";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=cambiarcontrasena&err&msj=$err';</script>";
      exit(1);
    }

    $admin = new Admin();
    
    $id = $_SESSION['idusu'];
    if($_SESSION['tipo']=='admin')
      $tip = 1;
    else
      $tip = 2;
    $r = $admin->actualizarPassword($pn, $pa, $id, $tip);
    
    if($r==true){
      $err = "¡Contraseña actualizada!";
      echo "<script>window.location ='?op=cambiarcontrasena&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=cambiarcontrasena&err&msj=$err';</script>";
    }
    exit(1);


  }
?>
