<?php
  include_once("modelo/Cliente.php"); 

  if(isset($_POST) && isset($_POST['btg'])){

    $cod = $_POST['cod_cli'];
    $nom = $_POST['nom_cli'];
    $ape = $_POST['ape_cli'];
    $tel = $_POST['tel_cli'];
    $cor = $_POST['cor_cli'];
    $dir = $_POST['dir_cli'];

    
    if(strlen($cod) == 0){
      $err = "Debe llenar el campo código.";
    }else if(strlen($nom) < 2){
      $err = "El campo nombre debe tener al menos 2 carácteres";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=clientes&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new Cliente();

    $cliente->data["id"] = "";
    $cliente->data["cod_cli"] = $cod;
    $cliente->data["nom_cli"] = $nom;
    $cliente->data["ape_cli"] = $ape;
    $cliente->data["cor_cli"] = $cor;
    $cliente->data["tel_cli"] = $tel;
    $cliente->data["dir_cli"] = $dir;
    $cliente->data["raz_cli"] = "";
    $cliente->data["rif_cli"] = "";
    $cliente->data["fec_reg_cli"] = date("Y-m-d H:i:s");
    $r = $cliente->save();
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      $err = "¡Registró correctamente!";
      echo "<script>window.location ='?op=clientes&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "¡Código/Correo ya existe!";
      echo "<script>window.location ='?op=clientes&err&msj=$err';</script>";
      exit(1);
    }

  }else if(isset($_POST) && isset($_POST['btc'])){

    $cod = $_POST['cod_cli'];
    $nom = $_POST['nom_cli'];
    $ape = $_POST['ape_cli'];
    $tel = $_POST['tel_cli'];
    $cor = $_POST['cor_cli'];
    $dir = $_POST['dir_cli'];

    
    if(strlen($cod) == 0){
      $err = "Debe llenar el campo código.";
    }else if(strlen($nom) < 2){
      $err = "El campo nombre debe tener al menos 2 carácteres";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=clientes&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new Cliente();
    $cliente->data["cod_cli"] = $cod;
    $cliente->data["nom_cli"] = $nom;
    $cliente->data["ape_cli"] = $ape;
    $cliente->data["cor_cli"] = $cor;
    $cliente->data["tel_cli"] = $tel;
    $cliente->data["dir_cli"] = $dir;
    $cliente->data["raz_cli"] = "";
    $cliente->data["rif_cli"] = "";
    $id = $_POST['idc'];
    $r = $cliente->edit($id);
    if($r==true){
      $err = "¡Información actualizada!";
      echo "<script>window.location ='?op=clientes&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=clientes&id=$id&info&msj=$err';</script>";
    }
    exit(1);


  }else if(isset($_GET['id'])){
    $id = $_GET['id'];
    $cliente = new Cliente();
    $r = $cliente->findById($id);

    if($r==false){
      $err = "No existe ningún registro con el ID ($id).";
      echo "<script>window.location ='?op=clientes&err&msj=$err';</script>";
      exit(1);
    }else{
      $F = $r;
    }
  }else if(isset($_GET['el'])){
    $id = $_GET['el'];
    $cliente = new Cliente();
    if($cliente->removeById($id)){
      $err = "¡Eliminado correctamente!";
      echo "<script>window.location ='?op=clientes&info&msj=$err';</script>";
    }else{
      $err = "El registro no puede ser eliminado, tiene información en uso.";
      echo "<script>window.location ='?op=clientes&err&msj=$err';</script>";
    }
    exit(1);
  }

?>
