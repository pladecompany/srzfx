<?php
  include_once("modelo/Categoria.php"); 
  include_once("modelo/Conexion.php"); 

  if(isset($_POST) && isset($_POST['btg'])){

    $mod = $_POST['mod'];
    $des = "";

    if(strlen($mod) == 0){
      $err = "Debe llenar el campo categía.";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=categorias&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new Categoria();

    $cliente->data["id"] = "";
    $cliente->data["nom_cat"] = $mod;
    $cliente->data["des_cate"] = $des;

    $r = $cliente->save();
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      $err = "¡Registró correctamente!";
      echo "<script>window.location ='?op=categorias&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "¡Código/Correo ya existe!";
      echo "<script>window.location ='?op=categorias&err&msj=$err';</script>";
      exit(1);
    }

  }else if(isset($_POST) && isset($_POST['btc'])){

    $idn = $_POST['idn'];
    $mod = $_POST['mod'];
    $des = "";

    if(strlen($mod) == 0){
      $err = "Debe llenar el campo modelo.";
    }


    if(isset($err)){
      echo "<script>window.location ='?op=categorias&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new Categoria();

    $cliente->data["id"] = $idn;
    $cliente->data["nom_cat"] = $mod;
    $cliente->data["des_cate"] = $des;
    $id = $_POST['idn'];

    $r = $cliente->edit($id);
    if($r==true){
      $err = "¡Información actualizada!";
      echo "<script>window.location ='?op=categorias&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=categorias&id=$id&info&msj=$err';</script>";
    }
    exit(1);


  }else if(isset($_GET['id'])){
    $id = $_GET['id'];
    $cliente = new Categoria();
    $r = $cliente->findById($id);
    if($r==false){
      $err = "No existe ningún registro con el ID ($id).";
      echo "<script>window.location ='?op=categorias&err&msj=$err';</script>";
      exit(1);
    }else{
      $F = $r;
    }
  }else if(isset($_GET['el'])){
    $id = $_GET['el'];
    $cliente = new Categoria();
    if($cliente->removeById($id)){
      $err = "¡Eliminado correctamente!";
      echo "<script>window.location ='?op=categorias&info&msj=$err';</script>";
    }else{
      $err = "El registro no puede ser eliminado, tiene información en uso.";
      echo "<script>window.location ='?op=categorias&err&msj=$err';</script>";
    }
    exit(1);
  }

?>
