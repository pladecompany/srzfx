<?php 
    include_once("panel/modelo/Usuario.php"); 
    if(isset($_POST['bti'])){
      $u = $_POST['cor'];
      $p = $_POST['pas'];

      if(strlen($u) <= 4){
        echo "<script>window.location = '?er=2&usu';</script>";
        exit(1);
      }

      if(strlen($p) < 5){
        echo "<script>window.location = '?er=3&pas';</script>";
        exit(1);
      }

      $rl = $orm->iniciarSesion($u, $p);
      if($rl == false){
        echo "<script>window.location = '?log=0&er=1';</script>";
        session_destroy();
        exit(1);
      }else{
        if($rl['est_usu']==1){
          session_start();
          $_SESSION['login'] = true;
          $_SESSION['tipo'] = "user";
          $_SESSION['usuario'] = $rl['nom_usu'].' '.$rl['ape_usu'];
          $_SESSION['idusu'] = $rl['id'];

          $_SESSION['nom_usu'] = $rl['nom_usu'];
          $_SESSION['ape_usu'] = $rl['ape_usu'];
          $_SESSION['fec_nac_usu'] = $rl['fec_nac_usu'];
          $_SESSION['cor_usu'] = $rl['cor_usu'];
          $_SESSION['sex_usu'] = $rl['sex_usu'];
          $_SESSION['tel_usu'] = $rl['tel_usu'];
          $_SESSION['img_usu'] = $rl['img_usu'];
          $_SESSION['id_pais'] = $rl['id_pais'];
          echo "<script>window.location = 'panel/index.php';</script>";
          exit(1);
        }else{
          echo "<script>window.location = '?er=4&noacc';</script>";
          exit(1);
        }
      }
    }

    if(isset($_POST['btc'])){
      $dominio=$orm->obtenerDominio();
      $email = $_POST['email'];
      $pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
      $sql = "UPDATE usuarios SET pas_usu=md5('".$pass."') WHERE cor_usu='$email';";
      $r = $orm->editarPersonalizado($sql);
      if($r == true){
        $mensaje='  
        <div style="background-color: #ca1724 !important;padding:.5em !important;color: #fff !important;font-weight: bold !important;margin-top: -1rem !important;box-shadow: 0px 5px 5px 0px #3d3c3c !important;">NUEVA CONTRASEÑA</div>

        <center>
          <img src="'.$dominio.'/static/img/logo.png" style="width:30%;"><br>
          <p style="color: #000 !important;font-size: 14px !important;"><b>CONTRASEÑA</b> '.$pass.'</p>
        </center>';
      
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $orm->enviarCorreo($email, $mensaje, "NUEVA CLAVE DE ACCESO PARA SRZFX");

        echo "<script>window.location = '?errr=0';</script>";
        exit(1);
      }else{
        echo "<script>window.location = '?errr=1';</script>";
        exit(1);
      }
    }
    
    if(isset($_POST) && isset($_POST['btr'])){

      $nom = $_POST['nom'];
      $ape = $_POST['ape'];
      $tel = $_POST['tel'];
      $cor = $_POST['email'];
      $sex = $_POST['sex'];
      $fec = $_POST['fec'];
      $pas = $_POST['pas'];
      $pas2 = $_POST['pas2'];
      $pais = $_POST['pais'];
      $fechanac = date('Y-m-d');
      $validfecha = strtotime ( '-10 year' , strtotime ( $fechanac ) ) ;
      $validfecha = date ( 'Y-m-d' , $validfecha );
      $verror= '&nom='.$nom.'&ape='.$ape.'&tel='.$tel.'&cor='.$cor.'&sex='.$sex.'&fec='.$fec.'&pais='.$pais;
      
      if(strlen($nom) < 2){
        $err = "El campo nombre debe tener al menos 2 carácteres";
      }else if(strlen($ape) < 2){
        $err = "El campo apellido debe tener al menos 2 carácteres";
      }else if($validfecha < $fec){
        $err = "Debes tener mas de 10 años para registrarte";
      }else if(strlen($tel) < 10 || strlen($tel)>14){
        $err = "El campo teléfono debe tener un formato valido";
      }else if(strlen($pas) < 5){
        $err = "La contraseña debe tener mínimo 5 carácteres";
      }else if(strlen($pas2) < 5){
        $err = "La contraseña a confirmar debe tener mínimo 5 carácteres";
      }else if($pas != $pas2){
        $err = "Las contraseñas no coinciden";
      }

      if(isset($err)){
        echo "<script>window.location ='?err&msj=$err".$verror."';</script>";
        exit(1);
      }

      $usuario = new Usuario();
      $usuario->data["id"] = "";
      $usuario->data["nom_usu"] = $nom;
      $usuario->data["ape_usu"] = $ape;
      $usuario->data["fec_nac_usu"] = $fec;
      $usuario->data["cor_usu"] = $cor;
      $usuario->data["sex_usu"] = $sex;
      $usuario->data["id_pais"] = $pais;
      $usuario->data["pas_usu"] = $pas;
      $usuario->data["ser_int_usu"] = '';
      $usuario->data["est_usu"] = '0';
      $usuario->data["img_usu"] = '';
      $usuario->data["tel_usu"] = $tel;
      
      
      
      $r = $usuario->save();
      if($r->affected_rows == 1){
        //$id = $r->insert_id;
        $dominio=$orm->obtenerDominio();
        $archivo = "panel/confi/costos.json";
        $data = file_get_contents($archivo); 
        $cos = json_decode($data, true);
        $mensaje='  
          <div style="background-color: #ca1724 !important;padding:.5em !important;color: #fff !important;font-weight: bold !important;margin-top: -1rem !important;box-shadow: 0px 5px 5px 0px #3d3c3c !important;">BIENVENIDO A SRZFX</div>

          <center>
            <img src="'.$dominio.'/static/img/logo.png" style="width:30%;"><br>
          </center>
            <p style="color: #000 !important;font-size: 14px !important;">Hola '.$nom.' '.$ape.'</p>
            <p style="color: #000 !important;font-size: 14px !important;">¡Bienvenido a SRZfx! Tu cuenta se ha creado con exito.</p>
            <p style="color: #000 !important;font-size: 14px !important;">Tus datos de acceso son:</p>
            <p style="color: #000 !important;font-size: 14px !important;"><b>CORREO:</b> '.$cor.'</p>
            <p style="color: #000 !important;font-size: 14px !important;"><b>CONTRASEÑA:</b> '.$pas.'</p>
            <p style="color: #000 !important;font-size: 14px !important;"><b>'.$cos['costos']['descripcion'].'</b></p>
            <p style="color: #000 !important;font-size: 14px !important;"><b>Valor: '.$cos['costos']['valor'].'$</b></p>';
        echo $mensaje;
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $orm->enviarCorreo($cor, $mensaje, "BIENVENIDO SRZFX");

        $mensaje='  
          <div style="background-color: #ca1724 !important;padding:.5em !important;color: #fff !important;font-weight: bold !important;margin-top: -1rem !important;box-shadow: 0px 5px 5px 0px #3d3c3c !important;">NUEVO REGISTRO</div>

          <center>
            <img src="'.$dominio.'/static/img/logo.png" style="width:30%;"><br>
          </center>
            <p style="color: #000 !important;font-size: 14px !important;">Se ha creado una nueva cuenta en SRZFX con exito.</p>
            <p style="color: #000 !important;font-size: 14px !important;">Los datos del nuevo estudiante son:</p>
            <p style="color: #000 !important;font-size: 14px !important;"><b>NOMBRE:</b> '.$nom.'</p>
            <p style="color: #000 !important;font-size: 14px !important;"><b>APELLIDO:</b> '.$ape.'</p>
            <p style="color: #000 !important;font-size: 14px !important;"><b>CORREO:</b> '.$cor.'</p>
            <p style="color: #000 !important;font-size: 14px !important;"><b>TELÉFONO:</b> '.$tel.'</p>';
        echo $mensaje;
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $email='Srzfxacademy@gmail.com';
        $orm->enviarCorreo($email, $mensaje, "NUEVO REGISTRO EN SRZFX");
        $err = "¡Registró correctamente, revisa tu email para finalizar el registro!";
        echo "<script>window.location ='?info&msj=$err';</script>";
        exit(1);
      }else{
        $err = "¡Correo ya existe!";
        echo "<script>window.location ='?err&msj=$err".$verror."';</script>";
        exit(1);
      }

    }
    if(isset($_GET['nom'])){
      $nom = $_GET['nom'];
      $ape = $_GET['ape'];
      $tel = $_GET['tel'];
      $cor = $_GET['cor'];
      $sex = $_GET['sex'];
      $fec = $_GET['fec'];
      $pais = $_GET['pais'];
    }
  if(isset($_POST) && isset($_POST['btcon'])){
      $dominio=$orm->obtenerDominio();
      $nom = $_POST['name'];
      $cor = $_POST['email'];
      $asu = $_POST['subject'];
      $msj = $_POST['message'];
      //$verror= '&nomc='.$nom.'&corc='.$cor.'&asu='.$asu.'&msj='.$msj;
      
      if(strlen($nom) < 2){
        $err = "2";
      }else if(strlen($cor) < 5){
        $err = "3";
      }else if(strlen($asu) < 2){
        $err = "4";
      }
      if(isset($err)){
        echo "<script>window.location ='?error=".$err."#contacto';</script>";
        exit(1);
      } 
      
      $mensaje='  
        <div style="background-color: #ca1724 !important;padding:.5em !important;color: #fff !important;font-weight: bold !important;margin-top: -1rem !important;box-shadow: 0px 5px 5px 0px #3d3c3c !important;">FOUMULARIO CONTACTO</div>

        <center>
          <img src="'.$dominio.'/static/img/logo.png" style="width:30%;"><br>
          <p style="color: #000 !important;font-size: 14px !important;"><b>NOMBRE:</b> '.$nom.'</p>
          <p style="color: #000 !important;font-size: 14px !important;"><b>CORREO:</b> '.$cor.'</p>
          <p style="color: #000 !important;font-size: 14px !important;"><b>TITULO:</b> '.$asu.'</p>
          <p style="color: #000 !important;font-size: 14px !important;"><b>MENSAJE:</b> '.$msj.'</p>
        </center>';
      
      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
      $email='contacto@srzfxacademy.com';
      $orm->enviarCorreo($email, $mensaje, "FORMULARIO DE CONTACTO SRZFX");
      $err = "1";
      echo "<script>window.location ='?error=".$err."#contacto';</script>";
      exit(1);
      

    }
?>