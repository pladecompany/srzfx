<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Archivo{
    private $tabla = "biblioteca";
    public $data = [];
    public $orm = null;

    public function Archivo($tabla){
      $this->orm = new Orm(new Conexion());
      $tihs->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function fetchAll(){
      $sql = "SELECT *, P.id as idp FROM ".$this->tabla." P, categorias C WHERE P.id_categoria=C.id ORDER BY P.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchAllActivas(){
      $sql = "SELECT * FROM ".$this->tabla." WHERE est_pdf='1' ORDER BY id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchAllActivasByCategory($id){
      $sql = "SELECT * FROM ".$this->tabla." WHERE est_pdf='1' AND id_categoria='$id' ORDER BY id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchAllActivasByName($txt){
      $sql = "SELECT * FROM ".$this->tabla." WHERE est_pdf='1' AND tit_pdf like '%$txt%' ORDER BY id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }

  }
?>
