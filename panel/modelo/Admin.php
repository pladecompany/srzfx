<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Admin{
    private $tabla = "admins";
    public $data = [];
    public $orm = null;

    public function Admin(){
      $this->orm = new Orm(new Conexion());
    }

    public function login($u, $p){
      $sql = "SELECT * FROM admins WHERE cor_adm='$u' AND pas_adm=md5('$p');";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function actualizarPassword($n, $a, $ida, $op){
      if($op==1)
        $sql = "UPDATE admins SET pas_adm=md5('$n') WHERE id='$ida' AND pas_adm=md5('$a');";
      else
        $sql = "UPDATE usuarios SET pas_usu=md5('$n') WHERE id='$ida' AND pas_usu=md5('$a');";
      return $this->orm->editarPersonalizado($sql);
    }

    public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id=$id;";
      return $this->orm->editarPersonalizado($sql);
    }

    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";

      return $this->orm->insertarPersonalizado($sql);
    }

  }
?>
