<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Usuario{
    private $tabla = "usuarios";
    public $data = [];
    public $orm = null;

    public function Usuario(){
      $this->orm = new Orm(new Conexion());
    }

    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else{
          if($key=='pas_usu')
            $sql.= "MD5('$index')";
          else
            $sql.= "'$index'";
        }
          
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

    public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id=$id;";
      return $this->orm->editarPersonalizado($sql);
    }

  }
?>
