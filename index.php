<!DOCTYPE html>
<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	include_once('ruta.php');
	include_once("panel/modelo/Orm.php");
	include_once('panel/modelo/loginuser.php');
?>

<html>
	<head>	
		<meta charset="UTF-8">
		<meta name="description" content="SRZ te impulsa al mercado de forex con asesorías y educación en línea.">
		<meta name="keywords" content="Forex, asesorías, educación financiera, negocios, trading, mercadeo">
		<meta name="author" content="SRZ">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="msapplication-TileColor" content="#00bcd4">
		<meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">

		<link rel="shortcut icon" href="static/img/logo.ico" />
		<link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
		<title>SRZFX</title>

		<link rel="stylesheet" href="static/css/bootstrap.min.css">
		<link rel="stylesheet" href="static/css/style.css">
		<link rel="stylesheet" href="static/css/responsive.css">
		<link rel="stylesheet" href="static/css/font-awesome.min.css">
		<link rel="stylesheet" href="static/css/animate.css">
		<link rel="stylesheet" href="static/css/owl.carousel.css">
		<link rel="stylesheet" href="static/css/owl.theme.css">
		<link rel="stylesheet" href="static/css/cd-hero.css">
		<link id="style-switch" href="static/css/presets/preset3.css" media="screen" rel="stylesheet" type="text/css">
	</head>

	<body>

	<div class="body-inner">
		<header id="header" class="navbar-fixed-top header" role="banner">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<div class="navbar-brand navbar-bg">
							<a href="index.php">
								<img class="img-responsive" src="static/img/logo.png" alt="logo">
							</a> 
						</div>                   
					</div>

					<nav class="collapse navbar-collapse clearfix" role="navigation">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="#inicio">Inicio</a></li>
							<li><a href="#beneficios">Beneficios</a></li>
							<li><a href="#elegirnos">¿Por qué elegirnos?</a></li>
							<li><a href="#contacto">Contacto</a></li>
							<li><a href="#ingresar" id="loginmodal" data-toggle="modal" data-target="#md-ingresar">Ingresar</a></li>
							<li><a href="#registro" data-toggle="modal" data-target="#md-registro" id="mod-reg">Registro</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>

		<?php include_once($ruta); ?>

		<div id="md-ingresar" class="modal modalmedium fade" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="title-box-d">
							<h3 class="title-d" id="titulo_modulo">Ingresa tus datos</h3>
						</div>

						<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_modelo">
							<div class="row">
								<div class="col-md-12 mb-2">
									<div class="form-group">
										<label for="correo">Correo electrónico</label>
										<input type="text" name="cor" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu correo">
									</div>
								</div>

								<div class="col-md-12 mb-2">
									<div class="form-group">
										<label for="contraseña">Contraseña</label>
										<input type="password" name="pas" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña">
									</div>
								</div>
							</div>
							<?php 
								if(isset($_GET['er'])){
                                  $err = $_GET['er'];
                                  if($err == 2)
                                    $msj = "El correo debe tener más de 4 carácteres";
                                  else if($err == 3)
                                    $msj = "La contraseña debe tener mínimo 5 carácteres";
                                  else if($err == 1)
                                    $msj = "Los datos de acceso son incorrectos.";
                                  else if($err == 4)
                                    $msj = "No puedes acceder hasta que no aprueben tu usuario.";
                            ?>
							<h5 style="color:red;" id="msj-error"><?php echo $msj;?></h5>
							<?php } ?>

							<a href="#" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#md-recuperar" id="olvid-cla">¿Olvidaste tu contraseña?</a>
						
							<div class="modal-footer">
								<button type="submit" class="btn btn-primary solid" name="bti">Ingresar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div id="md-recuperar" class="modal modalmedium fade" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="title-box-d">
							<h3 class="title-d" id="titulo_modulo">Recuperar contraseña</h3>
						</div>

						<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_modelo_2">
							<div class="row">
								<div class="col-md-12 mb-2">
									<div class="form-group">
										<label for="correo">Correo electrónico</label>
										<input type="email" name="email" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu correo">
									</div>
								</div>
							</div>
							<?php 
								if(isset($_GET['errr'])){
                                  $err = $_GET['errr'];
                                  if($err == 1){
                                  	$msj = "El correo no existe";
                                  	$col = "color:red;";
                                  }else{
                                  	$msj = "Se ha cambiado tu contraseña, revisa tu correo electrónico";
                                  	$col = "color:green;";
                                  }
                                                                      
                            ?>
							<h5 style="<?php echo $col;?>" id="msj-error2"><?php echo $msj;?></h5>
							<?php } ?>
							<div class="modal-footer">
								<button type="submit" class="btn btn-primary solid" name="btc">Listo</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div id="md-registro" class="modal modalmedium fade" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="title-box-d">
							<h3 class="title-d" id="titulo_modulo">Registro</h3>
						</div>

						<form class="form-a" method="POST" action="" enctype="multipart/form-data">

							<div class="row">
								<div class="col-sm-12 col-md-6 mb-2">
									<div class="form-group">
										<label for="nombre">Nombre</label>
										<input value="<?php echo $nom; ?>" type="text" name="nom" required class="form-control form-control-lg form-control-a" placeholder="Ingresa tu nombre">
									</div>
								</div>

								<div class="col-sm-12 col-md-6 mb-2">
									<div class="form-group">
										<label for="apellido">Apellido</label>
										<input value="<?php echo $ape; ?>" type="text" name="ape" required class="form-control form-control-lg form-control-a" placeholder="Ingresa tu apellido">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12 col-md-6 mb-2">
									<div class="form-group">
										<label for="fecha">Fecha de nacimiento</label>
										<?php 
											$fechanac = date('Y-m-d');
											$validfecha = strtotime ( '-10 year' , strtotime ( $fechanac ) ) ;
											$validfecha = date ( 'Y-m-d' , $validfecha );
										?>
										<input value="<?php echo $fec; ?>" type="date" name="fec" max="<?php echo $validfecha;?>" required class="form-control form-control-lg form-control-a">
									</div>
								</div>

								<div class="col-sm-12 col-md-6 mb-2">
									<div class="form-group">
										<label for="sex">Sexo</label>
										<select name="sex" required class="form-control" required>
											<option value="">Seleccione</option>
											<option value="f" <?php if($sex=='f') echo 'selected'; ?>>Femenino</option>
											<option value="m" <?php if($sex=='m') echo 'selected'; ?>>Masculino</option>
										</select>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12 col-md-6 mb-2">
									<div class="form-group">
										<label for="teléfono">Teléfono</label>
										<input value="<?php echo $tel; ?>" type="number" name="tel" max="99999999999999" required class="form-control form-control-lg form-control-a" placeholder="Ingresa tu teléfono">
									</div>
								</div>
								<div class="col-sm-12 col-md-6 mb-2">
									<div class="form-group">
										<label for="pais">Pais</label>
										<select name="pais" required class="form-control" required>
											<option value="">Seleccione</option>
											<?php 
												$ru=$orm->consultaPersonalizada("SELECT * FROM paises; ");
												while ($f=$ru->fetch_assoc()) {
													$selec='';
													if($pais==$f['id'])
														$selec='selected';
											?>
												<option value="<?php echo $f['id']; ?>" <?php echo $selec; ?>><?php echo $f['pais']; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 mb-2">
									<div class="form-group">
										<label for="correo">Correo electrónico</label>
										<input value="<?php echo $cor; ?>" type="email" name="email" required class="form-control form-control-lg form-control-a" placeholder="Ingresa tu correo">
									</div>
								</div>

								<div class="col-md-6 mb-2">
									<div class="form-group">
										<label for="contraseña">Contraseña</label>
										<input type="password" name="pas" required class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña">
									</div>
								</div>

								<div class="col-md-6 mb-2">
									<div class="form-group">
										<label for="confirmar">Confirmar contraseña</label>
										<input type="password" name="pas2" required class="form-control form-control-lg form-control-a" placeholder="Confirma tu contraseña">
									</div>
								</div>
								<div class="col-md-12">
									<?php 
										include_once("panel/vistas/mensajes.php");                    
				                    ?>
                    			</div>
							</div>
						
							<div class="modal-footer">
								<button type="submit" class="btn btn-primary solid" name="btr">Registro</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<footer id="footer" class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-12 footer-widget">
						<img width="50%" src="static/img/logo.png" alt="logo">
						<h3 class="widget-title">SRZFX</h3>
						<div class="latest-post-items media">
							<div class="latest-post-content media-body">
								<h4>Te impulsa al mercado de forex con asesorías y educación en línea.</h4>
							</div>
						</div>

						<div class="latest-post-items media">
							<div class="latest-post-content media-body">
								<h4>Nuestra misión es enseñar a profundidad a cada uno de nuestros estudiantes como entender el mercado, las realidades del mismo y como sacar provecho operando en los mercados financieros. <br>Nuestra visión es que cada estudiante alcance la libertad financiera, esa que todas las personas sueñan con alcanzar..</h4>
							</div>
						</div>
					</div>

					<div class="col-md-4 col-sm-12 footer-widget footer-about-us"></div>

					<div class="col-md-4 col-sm-12 footer-widget footer-about-us">
						<h3 class="widget-title">CONTÁCTANOS</h3>
						<div class="row">
							<div class="col-md-12">
								<h4>Correo electrónico:</h4>
								<h5>contacto@srzfxacademy.com</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<section id="copyright" class="copyright angle">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<ul class="footer-social unstyled">
							<li>
								<a title="Instagram" href="https://www.instagram.com/joelsrz/?hl=es-la" target="_blank">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-instagram"></i></span>
								</a>
								<a title="Facebook" href="https://www.facebook.com/Srzfx/" target="_blank">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-facebook"></i></span>
								</a>
								<a title="Telegram" href="https://t.me/Srzfxgroup" target="_blank">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-paper-plane"></i></span>
								</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 text-center">
						<div class="copyright-info">
							&copy; Copyright 2019 SRZFXACADEMY <span>Desarrollado por <a href="https://pladecompany.com" target="_blank">pladecompany.com</a></span>
						</div>
					</div>
				</div>

			   <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix">
					<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
				</div>
			</div>
		</section>

		<script type="text/javascript" src="static/js/jquery.js"></script>
		<script type="text/javascript" src="static/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="static/js/owl.carousel.js"></script>
		<script type="text/javascript" src="static/js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="static/js/jquery.flexslider.js"></script>
		<script type="text/javascript" src="static/js/cd-hero.js"></script>
		<script type="text/javascript" src="static/js/wow.min.js"></script>
		<script type="text/javascript" src="static/js/smoothscroll.js"></script>
		<script type="text/javascript" src="static/js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="static/js/jquery.counterup.min.js"></script>
		<script type="text/javascript" src="static/js/custom.js"></script>
		<?php 
			if(isset($_GET['er']) || isset($_GET['error'])){		
		?>
			<script type="text/javascript">
				<?php 
					if(isset($_GET['er']))
						echo '$("#loginmodal").trigger("click");';
				?>
				setTimeout(function(){
					$("#msj-error").hide();
				}, 5000);
			</script>
		<?php 
			}
		?>
		<?php 
			if(isset($_GET['errr'])){
		?>
			<script type="text/javascript">
				$("#olvid-cla").trigger("click");
				setTimeout(function(){
					$("#msj-error2").hide();
				}, 5000);
			</script>
		<?php 
			}
		?>
		<?php 
			if(isset($_GET['err']) || isset($_GET['info'])){
		?>
			<script type="text/javascript">
				$("#mod-reg").trigger("click");
			</script>
		<?php 
			}
		?>
	</body>
</html>
