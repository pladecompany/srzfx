<section id="inicio" class="no-padding">	
	<div id="main-slide" class="cd-hero">
		<ul class="cd-hero-slider">
			<li class="selected">
				<div class="overlay2">
					<img class="" src="static/img/srz-2.jpg" alt="SRZFX">
				</div>
				<div class="cd-full-width">
					<h2>SRZFX disciplina y confluencia</h2>
					<h3>Aprende y desarrolla habilidades para hacer trading en los mercados financieros</h3>
					<a href="#registro" data-toggle="modal" data-target="#registro" class="btn btn-primary white cd-btn">Comenzar ahora</a>
					<a href="index.php#contacto" class="btn btn-primary solid cd-btn">Quiero saber más</a>
				</div>
			</li>

			<li>
				<div class="overlay2">
					<img class="" src="static/img/srz.jpg" alt="SRZFX">
				</div>
				<div class="cd-full-width">
					<h2>SRZFX CLASSROOM</h2>
					<h3>Te ofrecemos videos clases para tu aprendizaje</h3>
					<a href="#registro" data-toggle="modal" data-target="#registro" class="btn btn-primary white cd-btn">Comenzar ahora</a>
					<a href="index.php#contacto" class="btn btn-primary solid cd-btn">Quiero saber más</a>
				</div>
			</li>

			<li>
				<div class="overlay2">
					<img class="" src="static/img/srz-1.jpg" alt="SRZFX">
				</div>
				<div class="cd-full-width">
					<h2>SRZ Aprende e invierte a la vez</h2>
					<h3>A medida que vas aprendiendo puedes invertir más y más, mientras nos dedicamos a fortalecer tus conocimientos</h3>
					<a href="#registro" data-toggle="modal" data-target="#registro" class="btn btn-primary white cd-btn">Comenzar ahora</a>
					<a href="index.php#contacto" class="btn btn-primary solid cd-btn">Quiero saber más</a>
				</div>
			</li>
		</ul> 

		<div class="cd-slider-nav">
			<nav>
				<span class="cd-marker item-1"></span>
				<ul>
					<li><a><i class="fa fa-book"></i> Aprende</a></li>
					<li><a><i class="fa fa-video-camera"></i> Descubre</a></li>
					<li><a><i class="fa fa-bar-chart"></i> Invierte</a></li>
				</ul>
			</nav> 
		</div>
	</div>
</section>


<section id="beneficios" class="service angle">
	<div class="container">
		<div class="row">
			<div class="col-md-12 heading">
				<span class="title-icon pull-left"><i class="fa fa-bookmark"></i></span>
				<h2 class="title">BENEFICIOS <span class="title-desc">Descubre todo los beneficios que te ofrecemos</span></h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4 col-sm-12 wow fadeInDown" data-wow-delay=".5s">
					<div class="service-content text-center">
						<span class="service-icon icon-pentagon"><i class="fa fa-video-camera"></i></span>
						<h2>SRZFX classroom</h2>
						<h5>Video clases para tu educación y aprendizaje en linea</h5>
					</div>
				</div>

				<div class="col-md-4 col-sm-12 wow fadeInDown" data-wow-delay=".8s" >
					<div class="service-content text-center">
						<span class="service-icon icon-pentagon"><i class="fa fa-photo"></i></span>
						<h2>Proyecciones</h2>
						<h5>Contenido multimedia para complementar tus conocimientos</h5>
					</div>
				</div>

				<div class="col-md-4 col-sm-12 wow fadeInDown" data-wow-delay="1.1s">
					<div class="service-content text-center">
						<span class="service-icon icon-pentagon"><i class="fa fa-book"></i></span>
						<h2>Biblioteca</h2>
						<h5>Contenido descargable (Pdf) para que no pares de aprender</h5>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="elegirnos" class="image-block no-padding">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 ts-padding" style="height:650px;background:url(static/img/image-block-bg.jpg) 50% 50% / cover no-repeat;">
			</div>
			<div class="col-md-6 ts-padding img-block-right">
				<div class="img-block-head text-center">
					<h2>CONOCE MÁS ACERCA DE NUESTRO MÉTODO DE APRENDIZAJE</h2>
					<h3>¿Por qué elegirnos?</h3>
					<h5>Nuestro esquema de enseñanza está estructurado para que cualquier persona sin conocimientos previos pueda convertirse en un profesional en los mercados financieros
					</h5>
				</div>

				<div class="gap-30"></div>

				<div class="image-block-content">
					<span class="feature-icon pull-left" ><i class="fa fa-camera"></i></span>
					<div class="feature-content">
						<h3>Video clases</h3>
						<p>Contenido audio visual 24/7</p>
					</div>
				</div>

				<div class="image-block-content">
					<span class="feature-icon pull-left" ><i class="fa fa-support"></i></span>
					<div class="feature-content">
						<h3>Mentoría</h3>
						<p>Nuestro objetivo es acompañar cada paso que das</p>
					</div>
				</div>

				<div class="image-block-content">
					<span class="feature-icon pull-left" ><i class="fa fa-street-view"></i></span>
					<div class="feature-content">
						<h3>No importa donde te encuentres</h3>
						<p>Puedes acceder a todo nuestro contenido desde cualquier parte del mundo</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="feature" class="feature">
	<div class="container">
		<div class="row">
			<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
				<span class="feature-icon pull-left" ><i class="fa fa-heart-o"></i></span>
				<div class="feature-content">
					<h3>Diseño moderno</h3>
					<p>Nuestra web te permite aprender de forma facíl y con un diseño amigable</p>
				</div>
			</div>

			<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
				<span class="feature-icon pull-left" ><i class="fa fa-codepen"></i></span>
				<div class="feature-content">
					<h3>Herramientas</h3>
					<p>Clases, videos, imagenes, proyecciones, descargables, todo en un solo lugar</p>
				</div>
			</div>

			<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
				<span class="feature-icon pull-left" ><i class="fa fa-film"></i></span>
				<div class="feature-content">
					<h3>Reproducción de videos</h3>
					<p>Reproduce las clases que desees sin ningún limite</p>
				</div>
			</div>
		</div>

		<div class="gap-40"></div>

		<div class="row">
			<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
				<span class="feature-icon pull-left" ><i class="fa fa-newspaper-o"></i></span>
				<div class="feature-content">
					<h3>Horario flexible</h3>
					<p>Accede a nuestro contenido cuando quieras, no establecemos horarios ni limites</p>
				</div>
			</div>

			<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
				<span class="feature-icon pull-left" ><i class="fa fa-desktop"></i></span>
				<div class="feature-content">
					<h3>Dispositivos y conexiones</h3>
					<p>Puedes ingresar desde tu teléfono, tablet o computadora. Nuestro diseño se adapta a tus dispositivos</p>
				</div>
			</div>

			<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
				<span class="feature-icon pull-left" ><i class="fa fa-pagelines"></i></span>
				<div class="feature-content">
					<h3>Facíl y sencillo</h3>
					<p>No tienes que dar mil vueltas para acceder a nuestro contenido, todo en un solo lugar</p>
				</div>
			</div>
		</div>

		<div class="gap-40"></div>

		<div class="row">
			<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
				<span class="feature-icon pull-left" ><i class="fa fa-recycle"></i></span>
				<div class="feature-content">
					<h3>Adquiere la membresía </h3>
					<p>de nuestra academia realizando un pago único de por vida</p>
				</div>
			</div>

			<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
				<span class="feature-icon pull-left" ><i class="fa fa-diamond"></i></span>
				<div class="feature-content">
					<h3>Resultados</h3>
					<p>No tengas dudas, con nuestro esquema de enseñanza alcanzarás la rentabilidad</p>
				</div>
			</div>

			<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
				<span class="feature-icon pull-left" ><i class="fa fa-whatsapp"></i></span>
				<div class="feature-content">
					<h3>Soporte 24/7</h3>
					<p>Estaremos para ti cuando sea necesario, no dudes en escribirnos</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="parallax parallax1">
	<div class="parallax-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h2>¿Deseas empezar a invertir en tu futuro?</h2>
				<p>
					<a href="#registro" data-toggle="modal" data-target="#registro" class="btn btn-primary solid">Comenzar ahora</a>
					<a href="#contacto" class="btn btn-primary white">Quiero comunicarme</a>
				</p>
			</div>
		</div>
	</div>
</section>

<section id="contacto">
	<div class="container">
		<div class="row">
			<div class="col-md-12 heading">
				<span class="title-icon pull-left"><i class="fa fa-user"></i></span>
				<h2 class="title">Contacto <span class="title-desc">Escríbenos y cuéntanos tus dudas</span></h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-7">
				<form id="contact-form" action="" method="post" role="form">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Nombre</label>
							<input class="form-control" name="name" id="name" placeholder="" type="text" required>
							</div>
						</div>
						
						<div class="col-md-4">
							<div class="form-group">
								<label>Correo</label>
								<input class="form-control" name="email" id="email" 
								placeholder="" type="email" required>
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								<label>Título</label>
								<input class="form-control" name="subject" id="subject" 
								placeholder="" required>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label>Mensaje</label>
						<textarea class="form-control" name="message" id="message" placeholder="" rows="10" required></textarea>
					</div>
					<?php 
						if(isset($_GET['error'])){
                            $err = $_GET['error'];
                            if($err == 1){
                              $msjc = "¡Se envio correctamente!";
                              $colc = 'color:green;';
                            }else if($err == 2){
                              $msjc = "El campo nombre debe tener al menos 2 carácteres";
                              $colc = 'color:red;';
                            }else if($err == 3){
                              $msjc = "Debes ingresar un correo";
                              $colc = 'color:red;';
                            }else if($err == 4){
                              $msjc = "El campo titulo debe tener al menos 2 carácteres";
                              $colc = 'color:red;';
                            }
                    ?>
					<h5 style="<?php echo $colc;?>" id="msj-error"><?php echo $msjc;?></h5>
					<?php } ?>
					<div class="text-right"><br>
						<button class="btn btn-primary solid blank" name="btcon" type="submit">Enviar mensaje</button> 
					</div>
				</form>
			</div>

			<div class="col-md-5">
				<div class="contact-info text-center">
					<h3>Contacto</h3>
					<br>
					<p><i class="fa fa-phone info"></i>  +(58) 238-4131 </p>
					<p><i class="fa fa-envelope-o info"></i>  contacto@srzfxacademy.com</p>
					<p><i class="fa fa-globe info"></i>  <a href="http://www.srzfxacademy.com/" target="_blank">www.srzfxacademy.com</a></p>
					<img width="50%" src="static/img/logo.png" alt="logo">
				</div>
			</div>
		</div>
	</div>
</section>

